package com.infinitemind.usoschedule.model;

public class Average {

	public int count;
	public float sum;

	public float get() {
		return sum / count;
	}
}
