package com.infinitemind.usoschedule.model;

import android.support.annotation.NonNull;

import com.infinitemind.usoschedule.utils.AppPreferences;
import com.infinitemind.usoschedule.views.WeekView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

public class Course extends WeekView.WeekViewEvent {

    public Date start_time;
    public Date end_time;
    private Name name;
    public String courseId;
    public Name buildingName;
    public String buildingId;
    public String roomNumber;
    public String groupNumber;
    public long roomId;
    public String frequency;
    private String unitId;
    public String classtypeId;
    public Name classtypeName;
    public String lecturerName;
    public List<String> lecturerIds;
	public String lecturerEmail;

    public String getCourseName() {
        return name.pl;
    }

    private String getCourseId() {
        return courseId + "_" + unitId + "_" + classtypeId;
    }

    public static WeekView.WeekViewEvent toWeekViewEvent(AppPreferences prefs, Course c) {
        WeekView.WeekViewEvent viewEvent = new WeekView.WeekViewEvent();
        viewEvent.setId(c.getCourseId().hashCode());
        Calendar startTime = new GregorianCalendar();
        startTime.setTime(c.start_time);
        viewEvent.setStartTime(startTime);
        Calendar endTime = new GregorianCalendar();
        endTime.setTime(c.end_time);
        viewEvent.setEndTime(endTime);
        viewEvent.setName(String.format("%s (%s)", c.getCourseName(), c.roomNumber));
        int color = prefs.getEventColorForClassType(c.classtypeName.getName());
        viewEvent.setColor(color);
        return viewEvent;
    }

    public static Course getCourse(List<Course> allCourses, WeekView.WeekViewEvent event) {
        for(Course c : allCourses)
            if(event.getId() == c.getCourseId().hashCode())
                return c;
        return null;
    }

    public static boolean containsWeekViewEvent(List<WeekView.WeekViewEvent> courses, WeekView.WeekViewEvent event) {
        for(WeekView.WeekViewEvent weekViewEvent : courses)
            if(weekViewEvent.getStartTime().getTimeInMillis() == event.getStartTime().getTimeInMillis() &&
                    weekViewEvent.getEndTime().getTimeInMillis() == event.getEndTime().getTimeInMillis() &&
                    weekViewEvent.getName().equals(event.getName()) &&
                    weekViewEvent.getColor() == event.getColor())
                return true;
        return false;
    }

    @NonNull @Override public String toString() {
        StringBuilder string = new StringBuilder();
        string.append(name.getName()).append(", ");
        string.append(new SimpleDateFormat("dd.MM HH:mm", Locale.getDefault()).format(start_time)).append(", ");
        string.append(new SimpleDateFormat("dd.MM HH:mm", Locale.getDefault()).format(end_time)).append(", ");
        return string.toString();
    }
}
