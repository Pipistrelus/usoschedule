package com.infinitemind.usoschedule.model;

public class Building {

	public String id;
	public Name name;
	public Name campus_name;
	public String postal_address;
	public LatLong location;
}
