package com.infinitemind.usoschedule.model;

public class CourseGrade {
	public String course_id;
	public String value_symbol;
	public String date_modified;
	public boolean counts_into_average;
	public int exam_session_number;
	public int exam_id;
}
