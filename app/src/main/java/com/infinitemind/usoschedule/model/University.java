package com.infinitemind.usoschedule.model;

public class University {

    public final String name;
    public final String location;
    public final String url;
    public final String consumerKey;
    public final String consumerSecret;

    public University(String name, String location, String url, String consumerKey, String consumerSecret) {
        this.name = name;
        this.location = location;
        this.url = url;
        this.consumerKey = consumerKey;
        this.consumerSecret = consumerSecret;
    }

    public String getServiceUrl() {
        return url + "/services";
    }

    public int compareTo(University u2) {
        int c1 = location.compareTo(u2.location);
        return c1 == 0 ? name.compareTo(u2.name) : c1;
    }
}
