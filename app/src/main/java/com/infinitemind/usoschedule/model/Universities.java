package com.infinitemind.usoschedule.model;

import com.infinitemind.usoschedule.utils.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Universities {
	private static final University[] universities = new University[]{
			new University("Politechnika Rzeszowska",
					"Rzeszów", "https://usosapps.prz.edu.pl",
					"V6bjNvDEhZ2xbVEyrwmP",
					"ZjPXVRhQpEMBzcuTdCDLZaZdmGzYvaZwUVkZNnTD"),

			new University("Chrześcijańska Akademia Teologiczna",
					"Warszawa", "https://usosapps.chat.edu.pl",
					"qDYhYxehRC3H6g7B9gaT",
					"gNPNj5mdBRrAdKjaQh2dfwyBRgu5Wq6nVTLzRbpj"),

			new University("Elbląska Uczelnia Humanistyczno-Ekonomiczna",
					"Elbląg", "https://usosapps.euh-e.edu.pl",
					"5a4FwDadcAtCZ5xw86Vk",
					"AVNTkjHHdLAQr2vKcwqAFbLYMUj3KM426kzMeqEs"),

			new University("Państwowa Wyższa Szkoła Informatyki i Przedsiębiorczości",
					"Łomża", "https://api.pwsip.edu.pl/usosapps",
					"VycqS2cf5EPcWqnesWcX",
					"bZRxPahf4gBr8ygQ87wyyXqpGFMZEQmK8tKvYvED"),

			new University("Akademia Teatralna im. Aleksandra Zelwerowicza",
					"Warszawa", "https://usosapps.at.edu.pl",
					"2hnN325P7CuNMZbGb8ZC",
					"ZAU3Xfq7RE3kJTBppSHbyW2qkHbbaqXLa3w2srLQ"),

			new University("Państwowa Wyższa Szkoła Zawodowa",
					"Elbląg", "https://usosapps.pwsz.elblag.pl",
					"4bAKc9RHSNtf8FkZcseZ",
					"7WV3hcdDpzs76kZJ2puz5pGZE2AYdEGdbkGkLXF6"),

			new University("Akademia Sztuk Pięknych",
					"Katowice", "https://apps.asp.katowice.pl",
					"V7Ws7xcje8JVVnF4Gu4A",
					"NbbdzX5eckgywZBLMAVLnYZCbCCNC58CptWU7hjB"),

			new University("Uniwersytet im. Adama Mickiewicza",
					"Poznań", "https://usosapps.amu.edu.pl",
					"AYKTjy2KurfJLeAwwz3x",
					"ymkJWU8eJQGhzK7RWd9dYzr4uqV2Gctf9t5AnZeL"),

			new University("Politechnika Białostocka",
					"Białystok", "https://api.uci.pb.edu.pl",
					"rry3sgJv8Qkqvh8jD6JF",
					"jeGyGn5sEHB4eqxcMtZBzdmn2MNdgGFwxQShBa6K"),

			new University("Uniwersytet Jagielloński",
					"Kraków", "https://apps.usos.uj.edu.pl",
					"Fhdm6LgPrNw8UjDPknzV",
					"C4EFJ2kW99ussnDzd8gcSXkPvZuWy8XP9fbbHJK7"),

			new University("Uniwersytet Marii Curie-Skłodowskiej",
					"Lublin", "https://apps.umcs.pl",
					"67XqWPg7gfQvPJSGucFW",
					"QsL58HcjZ2Wb843eKhKaTyWxcUWVnTc3HFzsFHxv"),

			new University("Uniwersytet Mikołaja Kopernika",
					"Toruń", "https://usosapps.umk.pl",
					"UJZ3v9HjURHdLv5mzVxD",
					"UkwnZffz3Hnc2X4X62grBHqZWVXtaELq2BJEybsh"),

			new University("Politechnika Warszawska",
					"Warszawa", "https://apps.usos.pw.edu.pl",
					"CFP7PFuUdv8GyTLEN5HH",
					"CB9nLPLxmfePebTD5p6sm3fgh5zKmaj9tbb6k69R"),

			new University("Uniwersytet Ekonomiczny",
					"Wrocław", "https://usosapps.ue.wroc.pl",
					"cc7HxaRGavfjzm5W3uXE",
					"5mRrmJAX8QrLNLJb8ReJucBbshjYeUaPCvdYJKK3"),

			new University("Uniwersytet Śląski w Katowicach",
					"Katowice", "https://usosapps.us.edu.pl",
					"DTFhC3F5wheNpathVyXr",
					"dBfe5eh6kT3pEPCWgzBeYyr9fxH8zLpBGXERRW2s"),

			new University("Wyższa Szkoła Ekonomiczna",
					"Białystok", "https://usosapps.wse.edu.pl",
					"C79mrBS4uT5snR3rKbVZ",
					"2NNJkNnMUF9ZuvUshsPqUVyreja2nN6EnWNwzRwm"),

			new University("Uniwersytet Kardynała Stefana Wyszyńskiego",
					"Warszawa", "https://apps.usos.uksw.edu.pl",
					"WssMSkLvhXAcuXhrVa2k",
					"JMhGdKKY9dKsc3K67ebSrgFj4fcdWwQavUdb4xun"),

			new University("Akademia Muzyczna im. Feliksa Nowowiejskiego",
					"Bydgoszcz", "https://www.usosapi.amuz.bydgoszcz.pl",
					"AP4EUFctvdBamJrqfGSG",
					"hBHFAd8dfrfUqYR9dWxfkadJm6nuNrtJjdnk9Nhs"),

			new University("Akademia Wychowania Fizycznego im. Jerzego Kukuczki",
					"Katowice", "https://usosapps.awf.katowice.pl",
					"ERamc4XBgJ2rZp4D32UR",
					"SzB8KcvHEw74TGmcyUzUZ4pVAFP5YtMftBqpST2H"),

			new University("Uniwersytet Kazimierza Wielkiego",
					"Bydgoszcz", "https://api.ukw.edu.pl",
					"veqf2C8TqLTTuqfjwUcz",
					"fXA69GT8wycNpnYyjnGr9c7PzPMuaauGmPbdBDDA"),

			new University("Politechnika Świętokrzyska",
					"Kielce", "https://api.usos.tu.kielce.pl",
					"3VTecz4nm8HwXXkFEBPF",
					"b6VcDnkaQ4cDpKaJ9BGMyQ937jPxuAvhqX9h7h3F"),

			new University("Wojskowa Akademia Techniczna im. Jarosława Dąbrowskiego",
					"Warszawa", "https://usosapps.wat.edu.pl",
					"ew4xCNvDkpzRHJa79GTR",
					"f5SuaN65qy2CsTZ6wCWKBRkdbbE5R5jY8JSKaVdX"),

			new University("Uniwersytet Przyrodniczo-Humanistyczny",
					"Siedlce", "https://apps.uph.edu.pl",
					"VsYD9MMpwcd7b7ZdD2Cv",
					"WdstDgh9sAykjTTkQWnkegnsXAJFyEqW9KxW7DdZ"),

			new University("Państwowa Wyższa Szkoła Zawodowa im. Stanisława Pigonia",
					"Krosno", "https://usosapps.pwsz.krosno.pl",
					"ttYSckNtBJAEpSzYQEUv",
					"PJz7gNUTDvCYrnFvkUBXf2fdyHCRzPsw4MTAunEk"),

			new University("Uczelnia Techniczno-Handlowa im. Heleny Chodkowskiej",
					"Warszawa", "https://usosapps.uth.edu.pl",
					"aWE6RDYBUBawDvCr9SqW",
					"Sxb3GcnYsdpztJNStnSQaRCB422MNzN8BpKWaSbD"),

			new University("Uniwersytet w Białymstoku",
					"Białystok", "https://usosapps.uwb.edu.pl",
					"Mbnq6yLTfjMD5ncvf4bx",
					"Zg4CE6rFDNmB9RgzzW5F5GCQLrFkNwT2Kdx964Wq"),

			new University("Uniwersytet Opolski",
					"Opole", "https://usosapps.uni.opole.pl",
					"Xa2t9MXHX44xsbcq6QDH",
					"PtemKApNKzeA7pxSqDPEvaA9avGuhh5P2mKK26gk"),

			new University("Uniwersytet Technologiczno-Przyrodniczy im. Jana i Jędrzeja Śniadeckich",
					"Bydgoszcz", "https://usosapps.utp.edu.pl",
					"3fFL8asUc5JZTwrFwBLw",
					"zggZbneVYXjSNnKu7xHZDq4r6dHmDSvazHNPDh6c"),

			new University("Uniwersytet Warmińsko-Mazurski",
					"Olsztyn", "https://apps.uwm.edu.pl",
					"zabdXzDnBYc59QpChb8R",
					"HUEP5ZR9qW3hWhzuJvGb6TjkpKMmGGrWYWdyz5CW"),

			new University("Państwowa Wyższa Szkoła Zawodowa im. prezydenta Stanisława Wojciechowskiego",
					"Kalisz", "https://apps.pwsz.kalisz.pl",
					"VfrN7HQDHsfjfdqMa9DH",
					"MrRpQr8m2sTe9UKbxvWWUNVhd8V4y3sENEdLzZNm"),

			new University("Uniwersytet Muzyczny Fryderyka Chopina",
					"Warszawa", "https://usosapps.chopin.edu.pl/",
					"3JegW6RpbrZ8T599zBR4",
					"DAUjApd2t7Zbf2ua4AzJ5puqm8xFuXNkNCdRAWaW")
	};

	public static ArrayList<University> getUniversities() {
		ArrayList<University> universities = new ArrayList<>(Arrays.asList(Universities.universities));
		Collections.sort(universities, Utils.universitiesComparator);
		return universities;
	}

	public static University getUniversity(int id) {
		return getUniversities().get(id);
	}
}
