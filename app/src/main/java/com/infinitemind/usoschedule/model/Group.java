package com.infinitemind.usoschedule.model;

import java.util.List;

public class Group {
	public String course_id;
	public Term[] term_ids;
	public Name class_type;
	public Name course_name;
	public int group_number;
	public List<User> participants;
	public List<User> lecturers;
}
