package com.infinitemind.usoschedule.model;

public class StudentProgramme {

	public Programme programme;
	public String status;
	public String admission_date;
	public String id;

	public static class Programme {
		public Name description;
	}
}
