package com.infinitemind.usoschedule.model;

import java.util.List;

public class User {

    public String id;
    public Title titles;
    public Name office_hours;
    public Name interests;
    public String email;
    private String first_name;
    private String last_name;
    public String student_number;
    public String matched;
    public int student_status;
    public List<StudentProgramme> student_programmes;

    public String getUserName() {
        return getUserName(false);
    }

    public String getUserName(boolean reversed) {
        String output = String.format("%s %s", reversed ? last_name : first_name, reversed ? first_name : last_name);
        if(titles != null) {
            if(titles.before != null && !titles.before.isEmpty())
                output = String.format("%s %s", titles.before, output);
            if(titles.after != null && !titles.after.isEmpty())
                output = String.format("%s %s", output, titles.after);
        }
        return output;
    }

    public static class Title {
        public String before;
        public String after;
    }
}
