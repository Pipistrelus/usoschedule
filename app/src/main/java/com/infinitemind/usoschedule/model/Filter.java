package com.infinitemind.usoschedule.model;

public class Filter {

	private boolean checked;
	private boolean checkable;
	private String value;
	private String id;
	private int color;

	public Filter(String value, int color) {
		this(value, value, color);
		checkable = false;
	}

	public Filter(String value, String id, int color) {
		this.value = value;
		this.id = id;
		this.color = color;
		this.checked = this.checkable = true;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getColor() {
		return color;
	}

	public void setColor(int color) {
		this.color = color;
	}

	public boolean isCheckable() {
		return checkable;
	}

	public void setCheckable(boolean checkable) {
		this.checkable = checkable;
	}
}
