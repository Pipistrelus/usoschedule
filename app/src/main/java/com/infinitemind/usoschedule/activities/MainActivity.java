package com.infinitemind.usoschedule.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatDelegate;

import com.infinitemind.usoschedule.fragments.SettingsFragment;
import com.infinitemind.usoschedule.utils.AppPreferences;
import com.infinitemind.usoschedule.R;
import com.infinitemind.usoschedule.clickListeners.OnGroupClickListener;
import com.infinitemind.usoschedule.clickListeners.OnUserClickListener;
import com.infinitemind.usoschedule.fragments.BuildingsFragment;
import com.infinitemind.usoschedule.fragments.GroupFragment;
import com.infinitemind.usoschedule.fragments.ProfileFragment;
import com.infinitemind.usoschedule.fragments.SearchFragment;
import com.infinitemind.usoschedule.fragments.TimetableFragment;
import com.infinitemind.usoschedule.model.Group;
import com.infinitemind.usoschedule.model.User;
import com.irozon.justbar.JustBar;

import java.util.ArrayList;

public class MainActivity extends FragmentActivity implements OnUserClickListener, OnGroupClickListener {

	private final Fragment[] fragments = new Fragment[5];
	private Fragment currentFragment;
	private JustBar bottomBar;
	private ArrayList<Integer> history;

	@Override
    protected void onCreate(Bundle savedInstanceState) {
		AppPreferences prefs = new AppPreferences(this);
		if(prefs.isNightModeEnabled())
			setTheme(R.style.AppTheme_Dark);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		if(!prefs.hasAccessToken()) {
		    startActivity(new Intent(this, LoginActivity.class));
		    finish();
	    } else switchFragment(fragments[0] == null ? fragments[0] = new TimetableFragment() : fragments[0]);

		bottomBar = findViewById(R.id.navigationBar);

		history = new ArrayList<>();
		history.add(0);

	    bottomBar.setOnBarItemClickListener((item, pos) -> {
		    history.remove((Integer) pos);
		    history.add(pos);
		    switch(pos) {
			    case 0:
				    switchFragment(fragments[0] == null || ((TimetableFragment) fragments[0]).getUser() != null ? fragments[0] = new TimetableFragment() : fragments[0]);
				    break;
			    case 1:
				    switchFragment(fragments[1] == null ? fragments[1] = new SearchFragment().setOnUserClickListener(this) : fragments[1]);
				    break;
			    case 2:
				    switchFragment(fragments[2] == null ? fragments[2] = new BuildingsFragment() : fragments[2]);
				    break;
			    case 3:
					switchFragment(fragments[3] == null ? fragments[3] = new ProfileFragment().setOnCourseClickListener(this) : fragments[3]);
				    break;
			    case 4:
				    switchFragment(fragments[4] == null ? fragments[4] = new SettingsFragment().setClassTypes(((TimetableFragment) fragments[0]).getClassTypes()) : fragments[4]);
				    break;
		    }
	    });
    }

	private void switchFragment(Fragment fragment) {
	    FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
	    fragmentTransaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
	    if(currentFragment != null) fragmentTransaction.replace(R.id.container, currentFragment = fragment).commit();
	    else fragmentTransaction.add(R.id.container, currentFragment = fragment).commit();
    }

	@Override
	public void onGroupClick(Group group) {
		switchFragment(new GroupFragment().setGroup(group));
	}

	@Override
	public void onUserClick(User user) {
		bottomBar.setSelected(0);
		((TimetableFragment) fragments[0]).setUser(user);
		switchFragment(fragments[0]);
	}

	@Override
	public void onBackPressed() {
		if(history.size() <= 1 && currentFragment == fragments[0])
			super.onBackPressed();
		else if(history.size() <= 1) {
			bottomBar.setSelected(0);
			history.remove(history.size() - 1);
		} else {
			if(currentFragment != fragments[history.get(history.size() - 1)])
				bottomBar.setSelected(history.get(history.size() - 1));
			else {
				history.remove(history.size() - 1);
				bottomBar.setSelected(history.get(history.size() - 1));
			}
		}
	}
}
