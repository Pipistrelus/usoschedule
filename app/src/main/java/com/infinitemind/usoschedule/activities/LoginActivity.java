package com.infinitemind.usoschedule.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.customtabs.CustomTabsIntent;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;

import com.infinitemind.usoschedule.utils.AppPreferences;
import com.infinitemind.usoschedule.utils.GalleryLayoutManager;
import com.infinitemind.usoschedule.utils.Transformer;
import com.infinitemind.usoschedule.R;
import com.infinitemind.usoschedule.adapter.UniversityListAdapter;
import com.infinitemind.usoschedule.async.LoginAsync;
import com.infinitemind.usoschedule.model.Universities;
import com.tapadoo.alerter.Alerter;

public class LoginActivity extends Activity {

	public static final int CUSTOM_TABS_INTENT_REQUEST_CODE = 1;
	private static LoginActivity instance;
	private GalleryLayoutManager layoutManager;
	private AppPreferences prefs;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		if((prefs = new AppPreferences(this)).isNightModeEnabled())
			setTheme(R.style.AppTheme_Dark);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		instance = this;

		RecyclerView list = findViewById(R.id.universitiesList);
		list.post(() -> {
			layoutManager = new GalleryLayoutManager(getApplicationContext(), GalleryLayoutManager.VERTICAL);
			layoutManager.setItemTransformer(new Transformer());
			layoutManager.attach(list);
			list.setAdapter(new UniversityListAdapter(Universities.getUniversities()));
		});
	}

	public void clickContinue(View view) {
		prefs.edit().setUniversityId(layoutManager.getCurSelectedPosition()).commit();
		new LoginAsync(this, url -> {
			if(TextUtils.isEmpty(url)) {
				Typeface arconFont = ResourcesCompat.getFont(getApplicationContext(), R.font.arcon);
				if(arconFont != null) Alerter.create(this)
						.setBackgroundColorRes(R.color.colorPrimary)
						.setTitle(getResources().getString(R.string.message))
						.setText(getResources().getString(R.string.connection_warning))
						.enableSwipeToDismiss()
						.setTitleTypeface(arconFont)
						.setTextTypeface(arconFont)
						.setDuration(3000)
						.show();
			} else {
				CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
				builder.setToolbarColor(getApplicationContext().getResources().getColor(R.color.colorPrimary));
				CustomTabsIntent build = builder.build();
				build.intent.setData(Uri.parse(url));
				build.intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
				startActivityForResult(build.intent, CUSTOM_TABS_INTENT_REQUEST_CODE);
			}
		}).execute();
	}

	public static LoginActivity getInstance() {
		return instance;
	}
}
