package com.infinitemind.usoschedule.activities;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatDelegate;

import com.infinitemind.usoschedule.async.GetAccessTokenAsync;
import com.infinitemind.usoschedule.utils.AppPreferences;
import com.infinitemind.usoschedule.R;

import org.scribe.extractors.TokenExtractorImpl;
import org.scribe.model.Token;

public class CallbackActivity extends Activity {

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		AppPreferences mAppPrefs = new AppPreferences(this);
		if(mAppPrefs.isNightModeEnabled())
			setTheme(R.style.AppTheme_Dark);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_callback);

		Uri uri = getIntent().getData();
		if(uri != null) {
			String token = uri.getQueryParameter("oauth_token");
			String verifier = uri.getQueryParameter("oauth_verifier");

			final String rawResponse = mAppPrefs.getTokenResponse(token);
			if(rawResponse != null && !rawResponse.isEmpty()) {
				Token requestToken = new TokenExtractorImpl().extract(rawResponse);

				new GetAccessTokenAsync(this, requestToken, verifier, () -> {
					finishActivity(LoginActivity.CUSTOM_TABS_INTENT_REQUEST_CODE);
					LoginActivity instance = LoginActivity.getInstance();
					if(instance != null) instance.finish();
					startActivity(new Intent(this, MainActivity.class));
					finish();
				}).execute();
			} else finish();
		}
	}
}
