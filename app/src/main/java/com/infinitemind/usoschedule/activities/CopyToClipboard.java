package com.infinitemind.usoschedule.activities;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.infinitemind.usoschedule.R;

public class CopyToClipboard extends Activity {
	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		CharSequence text = getIntent().getCharSequenceExtra(Intent.EXTRA_TEXT);
		ClipboardManager clipboardManager = (ClipboardManager)getSystemService(CLIPBOARD_SERVICE);
		if (clipboardManager != null) {
			clipboardManager.setPrimaryClip(ClipData.newPlainText(null, text));

			Toast.makeText(getApplicationContext(), getResources().getString(R.string.copied), Toast.LENGTH_SHORT).show();
		}
		finish();
	}
}
