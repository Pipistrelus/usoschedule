package com.infinitemind.usoschedule.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.text.Html;
import android.text.method.LinkMovementMethod;

import com.infinitemind.usoschedule.R;
import com.infinitemind.usoschedule.utils.AppPreferences;

public class MessageActivity extends Activity {

	public static final String MESSAGE_TAG = "message";

	@Override protected void onCreate(@Nullable Bundle savedInstanceState) {
		if(new AppPreferences(this).isNightModeEnabled())
			setTheme(R.style.AppTheme_Dark);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_message);

		setMessage();
	}

	private void setMessage() {
		Intent intent = getIntent();
		if(intent == null) return;

		String message = intent.getStringExtra(MESSAGE_TAG);
		if(message == null) return;


		AppCompatTextView messageView = findViewById(R.id.message);

		if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N)
			messageView.setText(Html.fromHtml(message, Html.FROM_HTML_MODE_LEGACY));
		else messageView.setText(Html.fromHtml(message));

		messageView.setMovementMethod(LinkMovementMethod.getInstance());
	}
}
