package com.infinitemind.usoschedule.fragments;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.Editable;
import android.transition.AutoTransition;
import android.transition.TransitionManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.infinitemind.usoschedule.R;
import com.infinitemind.usoschedule.adapter.BuildingListAdapter;
import com.infinitemind.usoschedule.adapter.FilterListAdapter;
import com.infinitemind.usoschedule.async.GetBuildingsAsync;
import com.infinitemind.usoschedule.model.Building;
import com.infinitemind.usoschedule.model.Filter;
import com.infinitemind.usoschedule.runnable.OnTextChanged;
import com.infinitemind.usoschedule.utils.UnScrollableLayoutManager;
import com.infinitemind.usoschedule.utils.Utils;
import com.tapadoo.alerter.Alerter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class BuildingsFragment extends Fragment {

	private final int MAX_FILTERS = 3;
	private ViewGroup rootView;
	private ArrayList<Building> allBuildings, buildings;
	private BuildingListAdapter buildingListAdapter;
	private StaggeredGridLayoutManager filterLayoutManager;
	private FilterListAdapter filterListAdapter;
	private ArrayList<Filter> filters;
	private boolean forceRefresh;

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		if(rootView == null || forceRefresh) {
			rootView = (ViewGroup) inflater.inflate(R.layout.fragment_buildings, container, false);

			new GetBuildingsAsync(buildings -> {
				if(buildings != null) {
					Collections.sort(buildings, Utils.buildingsComparator);
					allBuildings = new ArrayList<>(this.buildings = new ArrayList<>(buildings));

					setFiltersList();
					setBuildingsList();
					setOnScrollChangeListener();
					setTextChangedListener();

					rootView.findViewById(R.id.progressBar).setVisibility(View.GONE);

					TransitionManager.beginDelayedTransition(rootView, new AutoTransition());
				}
			}).execute(rootView.getContext());
		}

		if(forceRefresh = Utils.isOffline(rootView.getContext())) {
			Typeface arconFont = ResourcesCompat.getFont(rootView.getContext(), R.font.arcon);
			FragmentActivity activity = getActivity();
			if(arconFont != null && activity != null) {
				Alerter.create(activity)
						.setBackgroundColorRes(R.color.color1)
						.setTitle(getResources().getString(R.string.message))
						.setText(getResources().getString(R.string.connection_warning))
						.enableSwipeToDismiss()
						.setTitleTypeface(arconFont)
						.setTextTypeface(arconFont)
						.setDuration(3000)
						.show();
			}
		}
		return rootView;
	}

	private boolean meetsConditions(Building b) {
		String theRest = getResources().getString(R.string.the_rest);
		Editable text = ((AppCompatEditText)rootView.findViewById(R.id.editSearch)).getText();
		if(text == null) return false;

		String query = text.toString();

		ArrayList<String> ids = new ArrayList<>();
		for(Filter f : filters)
			if(!f.isChecked()) ids.add(f.getId());

		return (b.name.getName().contains(query) ||
				(b.postal_address != null && b.postal_address.contains(query)) ||
				(b.campus_name != null && b.campus_name.getName().contains(query))) &&
				(b.campus_name != null && !ids.contains(b.campus_name.getName()) ||
						b.campus_name == null && !ids.contains(theRest));
	}

	private void displayResult() {
		buildings.clear();
		for(Building b : allBuildings) if(meetsConditions(b)) buildings.add(b);

		buildingListAdapter.notifyDataSetChanged();
	}

	private void setTextChangedListener() {
		((AppCompatEditText) rootView.findViewById(R.id.editSearch)).addTextChangedListener(new OnTextChanged(editable -> {
			if(editable != null) {
				rootView.findViewById(R.id.buttonErase).setVisibility(editable.toString().isEmpty() ? View.GONE : View.VISIBLE);
				displayResult();
			}
		}));
		rootView.findViewById(R.id.buttonErase).setOnClickListener(view -> {
			Editable text = ((AppCompatEditText)rootView.findViewById(R.id.editSearch)).getText();
			if(text != null) text.clear();
			displayResult();
		});
	}

	private void setOnScrollChangeListener() {
		NestedScrollView scroll = rootView.findViewById(R.id.scrollView);
		scroll.getViewTreeObserver().addOnScrollChangedListener(() -> {
			if(filterListAdapter.isMaximized()) {
				View view = rootView.findViewById(R.id.filtersList);

				Context context = rootView.getContext();
				int diff = scroll.getScrollY() - (view.getMeasuredHeight() + Utils.dpToPx(context, 60));

				if(diff > 0) {
					float prevHeight = view.getMeasuredHeight();
					filterLayoutManager.setSpanCount(1);
					filterListAdapter.setMinimized(true);
					filterListAdapter.notifyDataSetChanged();
					view.post(() -> {
						view.invalidate();
						scroll.scrollBy(0, (int) (view.getMeasuredHeight() - prevHeight));
					});
				}
			}
		});
	}

	private void setFiltersList() {
		filters = new ArrayList<>();
		String theRest = rootView.getContext().getResources().getString(R.string.the_rest);
		HashMap<String, Boolean> exist = new HashMap<>();

		int color = rootView.getContext().getResources().getColor(R.color.color1);
		boolean undefined = false;
		for(Building b : buildings)
			if(b.campus_name != null && !exist.containsKey(b.campus_name.getName())) {
				exist.put(b.campus_name.getName(), true);
				filters.add(new Filter(b.campus_name.getName(), b.campus_name.getName(), color));
			} else if(b.campus_name == null) undefined = true;

		if(undefined) filters.add(new Filter(theRest, theRest, color));

		((RecyclerView) rootView.findViewById(R.id.filtersList)).setLayoutManager(filterLayoutManager = new StaggeredGridLayoutManager(1, LinearLayoutManager.HORIZONTAL));
		((RecyclerView) rootView.findViewById(R.id.filtersList)).setAdapter(filterListAdapter = new FilterListAdapter(filters, true));

		filterListAdapter.setOnCheckListener((position, checked) -> {
			if(filterListAdapter.isMaximized() || position != MAX_FILTERS) {
				filters.get(position).setChecked(checked);

				displayResult();
			} else {
				filterLayoutManager.setSpanCount(3);
				filterListAdapter.setMinimized(false);
				filterListAdapter.notifyDataSetChanged();
				TransitionManager.beginDelayedTransition(rootView, new AutoTransition());
			}
		});

		rootView.findViewById(R.id.filtersList).setVisibility(View.VISIBLE);
	}

	private void setBuildingsList() {
		((RecyclerView) rootView.findViewById(R.id.buildingsList)).setLayoutManager(new UnScrollableLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
		((RecyclerView) rootView.findViewById(R.id.buildingsList)).setAdapter(buildingListAdapter = new BuildingListAdapter(buildings));

		rootView.findViewById(R.id.buildingsList).setVisibility(View.VISIBLE);

		buildingListAdapter.setOnClickListener(position -> {
			Building building = buildings.get(position);
			Utils.showBuildingOnMap(getActivity(), building);
		});
	}
}