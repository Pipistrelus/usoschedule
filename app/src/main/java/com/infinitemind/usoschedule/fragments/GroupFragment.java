package com.infinitemind.usoschedule.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.AutoTransition;
import android.transition.TransitionManager;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.infinitemind.usoschedule.R;
import com.infinitemind.usoschedule.adapter.GradesListAdapter;
import com.infinitemind.usoschedule.adapter.UserListAdapter;
import com.infinitemind.usoschedule.async.GetCourseScheduleAsync;
import com.infinitemind.usoschedule.async.GetGradesAsync;
import com.infinitemind.usoschedule.async.GetUserDetailAsync;
import com.infinitemind.usoschedule.model.CourseGrade;
import com.infinitemind.usoschedule.model.Group;
import com.infinitemind.usoschedule.model.Name;
import com.infinitemind.usoschedule.model.User;
import com.infinitemind.usoschedule.utils.AppPreferences;
import com.infinitemind.usoschedule.utils.UnScrollableLayoutManager;
import com.infinitemind.usoschedule.utils.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

public class GroupFragment extends Fragment {

	private UserListAdapter participantsListAdapter;
	private ViewGroup rootView;
	private Group group;
	private AppPreferences prefs;

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		if(rootView == null) {
			rootView = (ViewGroup) inflater.inflate(R.layout.fragment_course, container, false);

			prefs = new AppPreferences(rootView.getContext());

			((AppCompatTextView) rootView.findViewById(R.id.topBarTitle)).setText(group.course_name.getName());
			((AppCompatTextView) rootView.findViewById(R.id.classType)).setText(group.class_type.getName());
			if(group.lecturers != null && !group.lecturers.isEmpty()) {
				((AppCompatTextView) rootView.findViewById(R.id.lecturer)).setText(group.lecturers.get(0).getUserName());
				setLecturer();
			}

			setGrades();
			setParticipantsList();

			handleClicks();
		}
		return rootView;
	}

	private void setLecturer() {
		User lecturer = prefs.getUserInfoById(group.lecturers.get(0).id);
		if(lecturer != null)
			showLecturerInfo(lecturer);
		else new GetUserDetailAsync(group.lecturers.get(0).id, user -> {
			if(user == null) return;

			showLecturerInfo(user);
			prefs.edit().setUserInfoById(user, user.id);
		}).execute(rootView.getContext());
	}

	private void showLecturerInfo(User user) {
		((AppCompatTextView) rootView.findViewById(R.id.lecturer)).setText(user.getUserName());

		String email = user.email;
		if(email != null && !email.isEmpty()) {
			((AppCompatTextView) rootView.findViewById(R.id.email)).setText(email);
			rootView.findViewById(R.id.emailContainer).setVisibility(View.VISIBLE);
			rootView.findViewById(R.id.titleEmail).setVisibility(View.VISIBLE);

			rootView.findViewById(R.id.emailContainer).setOnClickListener(view -> Utils.emailAt(rootView.getContext(), email));
		}

		Name office_hours = user.office_hours;
		if(office_hours != null && !office_hours.getName().isEmpty()) {
			((AppCompatTextView) rootView.findViewById(R.id.officeHours)).setText(office_hours.getName());
			rootView.findViewById(R.id.officeHoursContainer).setVisibility(View.VISIBLE);
			rootView.findViewById(R.id.titleOfficeHours).setVisibility(View.VISIBLE);
		}

		Name interests = user.interests;
		if(interests != null && !interests.getName().isEmpty()) {
			((AppCompatTextView) rootView.findViewById(R.id.interests)).setText(interests.getName());
			rootView.findViewById(R.id.interestsContainer).setVisibility(View.VISIBLE);
			rootView.findViewById(R.id.titleInterests).setVisibility(View.VISIBLE);
		}

		TransitionManager.beginDelayedTransition(rootView, new AutoTransition());
	}

	private void setGrades() {
		if(group.term_ids == null || group.term_ids.length < 1) return;

		List<CourseGrade> gradeList = prefs.getGrades(group.term_ids[group.term_ids.length - 1].id, Collections.singletonList(group.course_id));

		if(gradeList != null && !gradeList.isEmpty())
			showGrades(gradeList);
		else new GetGradesAsync(group.term_ids[group.term_ids.length - 1].id, Collections.singletonList(group.course_id), grades -> {
			if(grades == null || grades.isEmpty()) return;

			showGrades(grades);
			prefs.edit().setGrades(group.term_ids[group.term_ids.length - 1].id, grades);
		}).execute(rootView.getContext());
	}

	private void showGrades(List<CourseGrade> grades) {
		RecyclerView list = rootView.findViewById(R.id.gradesList);
		Collections.sort(grades, Utils.gradesComparator);
		list.setAdapter(new GradesListAdapter(new ArrayList<>(grades)));
		list.setNestedScrollingEnabled(false);
		list.setLayoutManager(new UnScrollableLayoutManager(rootView.getContext(), LinearLayoutManager.VERTICAL, false));

		rootView.findViewById(R.id.gradesContainer).setVisibility(View.VISIBLE);
		rootView.findViewById(R.id.titleGrades).setVisibility(View.VISIBLE);
		TransitionManager.beginDelayedTransition(rootView, new AutoTransition());
	}

	private void setParticipantsList() {
		RecyclerView list = rootView.findViewById(R.id.participantsList);
		Collections.sort(group.participants, Utils.userComparator);
		list.setAdapter(participantsListAdapter = new UserListAdapter(new ArrayList<>(group.participants), true));
		list.setNestedScrollingEnabled(false);
		list.setLayoutManager(new UnScrollableLayoutManager(rootView.getContext(), LinearLayoutManager.VERTICAL, false));

		((AppCompatTextView) rootView.findViewById(R.id.titleParticipants)).setText(String.format(Locale.getDefault(), "%s (%d)", getResources().getString(R.string.participants), group.participants.size()));

		rootView.findViewById(R.id.titleParticipants).setVisibility(View.VISIBLE);
		rootView.findViewById(R.id.participantsContainer).setVisibility(View.VISIBLE);

		TransitionManager.beginDelayedTransition(rootView, new AutoTransition());
	}

	private void handleClicks() {
		participantsListAdapter.setOnClickListener(new UserListAdapter.MyClickListener() {
			@Override public void onClick(int position) { }
			@Override public void onShowMoreClick() {
				participantsListAdapter.setMinimized(!participantsListAdapter.isMinimized());
			}
		});
	}

	public GroupFragment setGroup(Group group) {
		this.group = group;
		return this;
	}
}
