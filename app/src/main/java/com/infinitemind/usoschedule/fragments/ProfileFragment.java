package com.infinitemind.usoschedule.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.AutoTransition;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.infinitemind.usoschedule.R;
import com.infinitemind.usoschedule.adapter.GradesListAdapter;
import com.infinitemind.usoschedule.adapter.GroupsListAdapter;
import com.infinitemind.usoschedule.adapter.ProgrammeListAdapter;
import com.infinitemind.usoschedule.async.GetGradesAsync;
import com.infinitemind.usoschedule.async.GetUserDetailAsync;
import com.infinitemind.usoschedule.async.GetUserGroupsAsync;
import com.infinitemind.usoschedule.clickListeners.OnGroupClickListener;
import com.infinitemind.usoschedule.model.Average;
import com.infinitemind.usoschedule.model.CourseGrade;
import com.infinitemind.usoschedule.model.Group;
import com.infinitemind.usoschedule.model.User;
import com.infinitemind.usoschedule.runnable.OnTabSelected;
import com.infinitemind.usoschedule.utils.AppPreferences;
import com.infinitemind.usoschedule.utils.UnScrollableLayoutManager;
import com.infinitemind.usoschedule.utils.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class ProfileFragment extends Fragment {

	private OnGroupClickListener clickListener;
	private AppPreferences appPreferences;
	private ViewGroup rootView;
	private List<Group> groups;
	private List<CourseGrade> currentGrades;
	private List<List<CourseGrade>> grades;
	private HashMap<String, String> courseNames;
	private GroupsListAdapter groupsListAdapter;
	private GradesListAdapter gradesListAdapter;
	private List<Average> averages;
	private TabLayout termsTabLayout;

	private static int LOADED_INFO = 1;
	private static int LOADED_GRADES = 2;
	private static int LOADED_GROUPS = 4;
	private int loadingState = 0;

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		if(rootView == null) {
			rootView = (ViewGroup) inflater.inflate(R.layout.fragment_profile, container, false);

			appPreferences = new AppPreferences(rootView.getContext());

			termsTabLayout = rootView.findViewById(R.id.gradesTermsTabContainer);

			RecyclerView groupsList = rootView.findViewById(R.id.groupsList);
			groupsList.setLayoutManager(new UnScrollableLayoutManager(rootView.getContext(), LinearLayoutManager.VERTICAL, false));
			groupsList.setAdapter(groupsListAdapter = new GroupsListAdapter(groups = new ArrayList<>()));
			groupsList.setNestedScrollingEnabled(false);
			groupsListAdapter.setOnClickListener(position -> clickListener.onGroupClick(groups.get(position)));

			RecyclerView gradesList = rootView.findViewById(R.id.gradesList);
			gradesList.setLayoutManager(new UnScrollableLayoutManager(rootView.getContext(), LinearLayoutManager.VERTICAL, false));
			gradesList.setAdapter(gradesListAdapter = new GradesListAdapter(currentGrades = new ArrayList<>(), courseNames = new HashMap<>()));
			gradesList.setNestedScrollingEnabled(false);

			User userInfo = appPreferences.getUserInfo();
			if(userInfo != null) setUserInfo(userInfo);

			List<Group> groups = appPreferences.getGroups();
			if(groups != null) setGroups(groups);

			if(!Utils.isOffline(rootView.getContext())) {
				new GetUserDetailAsync(this::setUserInfo).execute(rootView.getContext());
				new GetUserGroupsAsync(g -> {
					setGroups(g == null ? null : g.get(g.size() - 1));
					setGrades(g);
				}).execute(rootView.getContext());
			}
		}

		return rootView;
	}

	private void setUserInfo(@Nullable User user) {
		if(user != null) {
			rootView.findViewById(R.id.progressBar).setVisibility(View.GONE);
			((AppCompatTextView)rootView.findViewById(R.id.name)).setText(user.getUserName());
			((AppCompatTextView)rootView.findViewById(R.id.studentNumber)).setText(String.format("%s", user.student_number));

			RecyclerView studentProgrammeList = rootView.findViewById(R.id.studentProgrammeList);
			Collections.sort(user.student_programmes, Utils.studentProgrammeComparator);
			studentProgrammeList.setLayoutManager(new UnScrollableLayoutManager(rootView.getContext(), LinearLayoutManager.VERTICAL, false));
			studentProgrammeList.setAdapter(new ProgrammeListAdapter(new ArrayList<>(user.student_programmes)));
			studentProgrammeList.setNestedScrollingEnabled(false);

			rootView.findViewById(R.id.studentProgrammeContainer).setVisibility(View.VISIBLE);
			rootView.findViewById(R.id.titleStudentProgramme).setVisibility(View.VISIBLE);

			TransitionManager.beginDelayedTransition(rootView, new AutoTransition());

			appPreferences.edit().setUserInfo(user);

			loadingState |= LOADED_INFO;
			checkLoadingState();
		}
	}

	private void setGroups(@Nullable List<Group> groups) {
		if(groups != null) {
			ArrayList<Group> list = new ArrayList<>(groups);
			Collections.sort(list, Utils.groupComparator);

			this.groups.clear();
			this.groups.addAll(list);

			groupsListAdapter.notifyDataSetChanged();

			rootView.findViewById(R.id.groupsContainer).setVisibility(View.VISIBLE);
			rootView.findViewById(R.id.titleGroups).setVisibility(View.VISIBLE);

			TransitionManager.beginDelayedTransition(rootView, new AutoTransition());

			appPreferences.edit().setGroups(list);

			loadingState |= LOADED_GROUPS;
			checkLoadingState();
		}
	}

	private void setGrades(@Nullable List<List<Group>> groups) {
		if(groups != null && !groups.isEmpty()) {
			currentGrades.clear();
			courseNames.clear();
			this.averages = new ArrayList<>();
			this.grades = new ArrayList<>();

			int numberOfTerms = groups.size();

			if(numberOfTerms > 1) setTermsTabLayout();

			for(int i = 0; i < numberOfTerms; i++) {
				if(numberOfTerms > 1) {
					TabLayout.Tab tab = termsTabLayout.newTab();
					tab.setCustomView(R.layout.item_tab);
					tab.setText(groups.get(i).get(0).term_ids[i].name.getName());
					termsTabLayout.addTab(tab, i == numberOfTerms - 1);
				}

				List<String> courseIds = new ArrayList<>();
				for(int j = 0; j < groups.get(i).size(); j++) {
					String course_id = groups.get(i).get(j).course_id;

					if(!courseNames.containsKey(course_id)) {
						courseNames.put(course_id, groups.get(i).get(j).course_name.getName());
						courseIds.add(course_id);
					}
				}

				int finalI = i;
				String termId = groups.get(i).get(0).term_ids[i].id;
				new GetGradesAsync(termId, courseIds, grades -> {
					if(grades == null) return;
					try {
						showGrades(grades, finalI);
					} catch(Exception e) {
						Log.e("LOG!", "Caught exception while setting grades...: " + e.getMessage());
					}
				}).execute(rootView.getContext());
			}
		}
	}

	private void setTermsTabLayout() {
		CourseGrade emptyGrades = new CourseGrade();
		emptyGrades.value_symbol = "-";
		termsTabLayout.setVisibility(View.VISIBLE);
		termsTabLayout.addOnTabSelectedListener(new OnTabSelected(tab -> {
			if(tab == null) return;

			currentGrades.clear();

			if(grades.size() > tab.getPosition() && grades.get(tab.getPosition()) != null && !grades.get(tab.getPosition()).isEmpty()) {
				currentGrades.addAll(grades.get(tab.getPosition()));
				showAverage(averages.size() > tab.getPosition() ? averages.get(tab.getPosition()) : null);
			} else {
				currentGrades.add(emptyGrades);
				showAverage(null);
			}

			gradesListAdapter.notifyDataSetChanged();
		}));
	}

	private void showGrades(List<CourseGrade> grades, int currentTerm) {
		if(this.grades.size() <= currentTerm || this.grades.get(currentTerm) == null)
			this.grades.add(new ArrayList<>());

		if(grades != null) {
			for(CourseGrade g : grades) {
				if(g.counts_into_average) {
					int index = g.value_symbol.indexOf(" ");
					String value_symbol = index != -1 ? g.value_symbol.substring(0, g.value_symbol.indexOf(" ")) : g.value_symbol;
					if(averages.size() <= currentTerm)
						averages.add(new Average());

					averages.get(currentTerm).count++;
					averages.get(currentTerm).sum += Float.parseFloat(value_symbol.replaceAll(",", "."));

					if(termsTabLayout.getSelectedTabPosition() == currentTerm)
						showAverage(averages.get(currentTerm));
				}
			}

			if(this.grades.size() > currentTerm)
				this.grades.get(currentTerm).addAll(grades);
			else this.grades.add(grades);
		}

		if(termsTabLayout.getSelectedTabPosition() == currentTerm && !this.grades.get(currentTerm).isEmpty() || termsTabLayout.getVisibility() != View.VISIBLE) {
			currentGrades.clear();
			currentGrades.addAll(this.grades.get(currentTerm));
			gradesListAdapter.notifyDataSetChanged();
		}

		if(this.currentGrades.size() == termsTabLayout.getSelectedTabPosition()) {
			rootView.findViewById(R.id.gradesContainer).setVisibility(View.VISIBLE);
			rootView.findViewById(R.id.titleGrades).setVisibility(View.VISIBLE);

			loadingState |= LOADED_GRADES;
			checkLoadingState();
		}

		TransitionManager.beginDelayedTransition(rootView, new AutoTransition());
	}

	private void checkLoadingState() {
		int loaded = LOADED_INFO | LOADED_GRADES | LOADED_GROUPS;
		if((loadingState & loaded) == loaded)
			rootView.findViewById(R.id.loadingIndicator).setVisibility(View.GONE);
	}

	private void showAverage(@Nullable Average average) {
		if(average != null) ((AppCompatTextView) rootView.findViewById(R.id.averageGrade)).setText(String.format(Locale.getDefault(), "%.2g", average.get()));
		else ((AppCompatTextView) rootView.findViewById(R.id.averageGrade)).setText("-");
	}

	public ProfileFragment setOnCourseClickListener(OnGroupClickListener clickListener) {
		this.clickListener = clickListener;
		return this;
	}
}
