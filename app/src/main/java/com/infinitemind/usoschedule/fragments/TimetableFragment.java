package com.infinitemind.usoschedule.fragments;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.transition.AutoTransition;
import android.transition.TransitionManager;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;

import com.infinitemind.usoschedule.activities.MainActivity;
import com.infinitemind.usoschedule.runnable.DataClassCallback;
import com.infinitemind.usoschedule.views.WeekView;
import com.infinitemind.usoschedule.views.WeekView.WeekViewEvent;
import com.infinitemind.usoschedule.utils.AppPreferences;
import com.infinitemind.usoschedule.R;
import com.infinitemind.usoschedule.adapter.FilterListAdapter;
import com.infinitemind.usoschedule.async.GetBuildingsAsync;
import com.infinitemind.usoschedule.async.GetUserDetailAsync;
import com.infinitemind.usoschedule.async.GetUserScheduleAsync;
import com.infinitemind.usoschedule.model.Course;
import com.infinitemind.usoschedule.model.Filter;
import com.infinitemind.usoschedule.model.User;
import com.infinitemind.usoschedule.utils.Utils;
import com.infinitemind.usoschedule.views.Chip;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class TimetableFragment extends Fragment {

	private Resources resources;
	private HashMap<String, List<WeekViewEvent>> allEvents;
	private Map<String, Boolean> loadedDays;
	private List<Course> allCourses;
	private ViewGroup rootView;
	private WeekView weekView;
	private boolean forceRefresh;
	private User user;
	private ArrayList<Filter> courses, classTypes, buildings;
	private AppPreferences prefs;

	private ObjectAnimator buttonSyncAnimator;
	private DataClassCallback<Boolean> callback1, callback2;

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
		if(rootView == null || user != null || forceRefresh) {
			rootView = (ViewGroup) inflater.inflate(R.layout.fragment_timetable, container, false);
			if(resources == null && rootView.getContext() != null)
				resources = rootView.getContext().getResources();
			forceRefresh = false;
			allEvents = new HashMap<>();
			allCourses = new ArrayList<>();
			loadedDays = new HashMap<>();
			classTypes = new ArrayList<>();
			courses = new ArrayList<>();
			buildings = new ArrayList<>();
			weekView = rootView.findViewById(R.id.weekView);
			prefs = new AppPreferences(rootView.getContext());

			toggleLoading(true);
			setData();
			updateData(null, () -> {
				toggleLoading(false);
				setFilters();
			});
			init();

			((AppCompatTextView) rootView.findViewById(R.id.topBarTitle)).setText(user != null ? user.getUserName() : resources.getString(R.string.timetable));

			weekView.setVisibleDays(prefs.getDefaultVisibleDays());

			handleClicks();
		} else {
			classTypes.clear();
			courses.clear();
			buildings.clear();

			setFilters();

			allEvents.clear();
			allCourses.clear();
			loadedDays.clear();
			updateData(weekView.getFirstVisibleDay(), null);
		}

		weekView.setEventTextSize(prefs.getEventTextSize());
		weekView.refreshDataset();

		return rootView;
	}

	private void setFilters() {
		int defaultColor = resources.getColor(R.color.colorDarkGrey);
		HashMap<String, Boolean> exist = new HashMap<>();
		for(int i = 0; i < allCourses.size(); i++) {
			Course c = allCourses.get(i);
			if(!exist.containsKey(c.courseId) && c.courseId != null) {
				exist.put(c.courseId, true);
				String filterName = c.getCourseName();
				if(filterName.contains(" - "))
					filterName = filterName.substring(0, filterName.lastIndexOf("-") - 1);
				courses.add(new Filter(filterName, c.courseId, defaultColor));
			} if(!exist.containsKey(c.classtypeId) && c.classtypeId != null) {
				exist.put(c.classtypeId, true);
				classTypes.add(new Filter(c.classtypeName.getName(), c.classtypeId, prefs.getEventColorForClassType(c.classtypeName.getName())));
			} if(!exist.containsKey(c.buildingId) && c.buildingId != null) {
				exist.put(c.buildingId, true);
				buildings.add(new Filter(c.buildingId, c.buildingId, defaultColor));
			}
		}

		rootView.findViewById(R.id.buttonFilter).setVisibility(View.VISIBLE);

		TransitionManager.beginDelayedTransition(rootView, new AutoTransition());
	}

	private void init() {
		weekView.setScrollListener((newFirstVisibleDay, oldFirstVisibleDay) -> updateData(newFirstVisibleDay, null));
	}

	private void handleClicks() {
		weekView.setEventClickListener((event, eventRect) -> showEvent(event));

		rootView.findViewById(R.id.buttonSync).setOnClickListener(view -> {
			allEvents.clear();
			allCourses.clear();
			loadedDays.clear();
			toggleLoading(true);
			updateData(weekView.getFirstVisibleDay(), () -> toggleLoading(false), !Utils.isOffline(rootView.getContext()));
		});

		rootView.findViewById(R.id.buttonCenter).setOnClickListener(view -> weekView.scrollToToday());

		rootView.findViewById(R.id.buttonSync).setOnLongClickListener(view -> {
			Utils.showTooltip(view);
			return true;
		});

		rootView.findViewById(R.id.buttonCenter).setOnLongClickListener(view -> {
			Utils.showTooltip(view);
			return true;
		});

		((Chip) rootView.findViewById(R.id.buttonFilter)).setOnCheckedChangeListener(ignored -> Utils.makeDialog(getActivity(), R.layout.dialog_show_filters, dialog -> {
			if(dialog != null) {
				ArrayList<Boolean> courses = new ArrayList<>(), classTypes = new ArrayList<>(), buildings = new ArrayList<>();
				saveFilterSettings(courses, TimetableFragment.this.courses);
				saveFilterSettings(classTypes, TimetableFragment.this.classTypes);
				saveFilterSettings(buildings, TimetableFragment.this.buildings);

				FilterListAdapter coursesAdapter = setFilterList(dialog, TimetableFragment.this.courses, R.id.studentProgrammeList);
				coursesAdapter.setOnCheckListener((position, checked) -> TimetableFragment.this.courses.get(position).setChecked(checked));
				FilterListAdapter classTypesAdapter = setFilterList(dialog, TimetableFragment.this.classTypes, R.id.classTypesList);
				classTypesAdapter.setOnCheckListener((position, checked) -> TimetableFragment.this.classTypes.get(position).setChecked(checked));
				FilterListAdapter buildingsAdapter = setFilterList(dialog, TimetableFragment.this.buildings, R.id.buildingsList);
				buildingsAdapter.setOnCheckListener((position, checked) -> TimetableFragment.this.buildings.get(position).setChecked(checked));

				dialog.setOnCancelListener(dialogInterface -> {
					restoreFilterSettings(courses, TimetableFragment.this.courses);
					restoreFilterSettings(classTypes, TimetableFragment.this.classTypes);
					restoreFilterSettings(buildings, TimetableFragment.this.buildings);
					coursesAdapter.notifyDataSetChanged();
					classTypesAdapter.notifyDataSetChanged();
					buildingsAdapter.notifyDataSetChanged();
					dialog.dismiss();
				});

				dialog.findViewById(R.id.okButton).setOnClickListener(v -> {
					allEvents.clear();
					setTimetable(allCourses, false);
					dialog.dismiss();
				});
			}
		}));
	}

	private void toggleLoading(boolean loading) {
		View buttonSync = rootView.findViewById(R.id.buttonSync);
		if(buttonSyncAnimator != null)
			buttonSyncAnimator.cancel();

		if(loading) {
			buttonSyncAnimator = ObjectAnimator.ofFloat(buttonSync, "rotation", 0, 360);
			buttonSyncAnimator.setRepeatCount(ValueAnimator.INFINITE);
			buttonSyncAnimator.setRepeatMode(ValueAnimator.RESTART);
			buttonSyncAnimator.setDuration(1000);
		} else {
			buttonSyncAnimator = ObjectAnimator.ofFloat(buttonSync, "rotation", buttonSync.getRotation(), 360);
			buttonSyncAnimator.setDuration((long)(buttonSync.getRotation() / 360 * 1000));
		}

		buttonSyncAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
		buttonSyncAnimator.start();
	}

	private void restoreFilterSettings(ArrayList<Boolean> settings, ArrayList<Filter> filters) {
		for(int i = 0; i < filters.size(); i++) filters.get(i).setChecked(settings.get(i));
	}

	private void saveFilterSettings(ArrayList<Boolean> settings, ArrayList<Filter> filters) {
		for(Filter f : filters) settings.add(f.isChecked());
	}

	private FilterListAdapter setFilterList(Dialog dialog, ArrayList<Filter> courses, int coursesList) {
		Collections.sort(courses, Utils.filterComparator);
		FilterListAdapter adapter = new FilterListAdapter(courses);
		((RecyclerView) dialog.findViewById(coursesList)).setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.HORIZONTAL));
		((RecyclerView) dialog.findViewById(coursesList)).setAdapter(adapter);
		return adapter;
	}

	private void showEvent(WeekViewEvent event) {
		Utils.makeDialog(getActivity(), R.layout.dialog_show_event, dialog -> {
			if(dialog != null) {
				AppCompatTextView titleView = dialog.findViewById(R.id.title);
				AppCompatTextView hourView = dialog.findViewById(R.id.hours);
				AppCompatTextView durationView = dialog.findViewById(R.id.duration);
				AppCompatTextView classroomView = dialog.findViewById(R.id.classroom);
				AppCompatTextView buildingView = dialog.findViewById(R.id.building);
				AppCompatTextView groupNameView = dialog.findViewById(R.id.groupName);
				AppCompatTextView classTypeView = dialog.findViewById(R.id.classType);
				AppCompatTextView lecturerView = dialog.findViewById(R.id.lecturer);
				AppCompatTextView emailView = dialog.findViewById(R.id.email);

				((ImageView) dialog.findViewById(R.id.icon1)).setColorFilter(event.getColor());
				((ImageView) dialog.findViewById(R.id.icon2)).setColorFilter(event.getColor());
				((ImageView) dialog.findViewById(R.id.icon3)).setColorFilter(event.getColor());
				((ImageView) dialog.findViewById(R.id.icon4)).setColorFilter(event.getColor());

				Course course = Course.getCourse(allCourses, event);

				long diff = event.getEndTime().getTimeInMillis() - event.getStartTime().getTimeInMillis();
				int hours = (int) TimeUnit.MILLISECONDS.toHours(diff), minutes = (int) TimeUnit.MILLISECONDS.toMinutes(diff - TimeUnit.HOURS.toMillis(hours));
				hourView.setText(String.format("%1$tk:%1$tM - %2$tk:%2$tM", event.getStartTime(), event.getEndTime()));
				durationView.setText(String.format(Locale.getDefault(), "%d %s %d %s", hours, resources.getQuantityString(R.plurals.hour, hours).toLowerCase(), minutes, resources.getQuantityString(R.plurals.minute, minutes).toLowerCase()));

				if(course != null) {
					titleView.setText(course.getCourseName());
					classroomView.setText(course.roomNumber);
					buildingView.setText(course.buildingName.getName());
					groupNameView.setText(course.groupNumber);
					classTypeView.setText(course.classtypeName.getName());

					User user = prefs.getUserInfoById(course.lecturerIds.get(0));
					if(user != null) {
						course.lecturerName = user.getUserName();
						course.lecturerEmail = user.email;
					}

					if(course.lecturerIds != null && !course.lecturerIds.isEmpty() && (course.lecturerName == null || course.lecturerName.isEmpty()))
						new GetUserDetailAsync(course.lecturerIds.get(0), lecturer -> {
							if(lecturer != null) {
								prefs.edit().setUserInfoById(lecturer, lecturer.id);

								lecturerView.setText(course.lecturerName = lecturer.getUserName());
								if(lecturer.email != null)
									emailView.setText(course.lecturerEmail = lecturer.email);

								dialog.findViewById(R.id.lecturerContainer).setVisibility(View.VISIBLE);
								TransitionManager.beginDelayedTransition(dialog.findViewById(R.id.background));
							}
						}).execute(rootView.getContext());
					else if(course.lecturerName != null && !course.lecturerName.isEmpty()) {
						lecturerView.setText(course.lecturerName);
						emailView.setText(course.lecturerEmail);

						dialog.findViewById(R.id.lecturerContainer).setVisibility(View.VISIBLE);
					}
					dialog.findViewById(R.id.classroomContainer).setOnClickListener(v -> new GetBuildingsAsync(course.buildingId, buildings -> {
						if(buildings != null && !buildings.isEmpty())
							Utils.showBuildingOnMap(getActivity(), buildings.get(0), event.getColor());
						else Utils.showBuildingOnMap(getActivity(), null, event.getColor());
					}).execute(rootView.getContext()));
				}

				emailView.setOnClickListener(v -> Utils.emailAt(rootView.getContext(), emailView.getText().toString()));

				dialog.findViewById(R.id.okButton).setOnClickListener(view -> dialog.dismiss());
			}
		});
	}

	private void setData() {
		weekView.setMonthChangeListener((newYear, newMonth) -> {
			String month = (newMonth < 10 ? "0" : "") + (newMonth + 1);
			String year = "" + newYear;
			List<WeekViewEvent> weekViewEvents = allEvents.get(String.format(Locale.getDefault(), "%s, %s", month, year));
			return weekViewEvents != null ? weekViewEvents : new ArrayList<>();
		});
	}

	private void updateData(@Nullable Calendar newFirstVisibleDay, @Nullable Runnable callback, boolean... forceSync) {
		Calendar day = Calendar.getInstance();
		day.set(Calendar.MILLISECOND, 0);
		day.set(Calendar.SECOND, 0);
		day.set(Calendar.MINUTE, 0);
		day.set(Calendar.HOUR_OF_DAY, 0);
		if(newFirstVisibleDay != null) day.setTime(newFirstVisibleDay.getTime());

		callback1 = new DataClassCallback<Boolean>() {
			@Override public void run(@Nullable Boolean data) {
				if(callback == null) return;

				if(callback2 != null && callback2.getObject() != null && (Boolean) callback2.getObject())
					callback.run();

				callback1.setObject(data);
			}
		};
		callback2 = new DataClassCallback<Boolean>() {
			@Override public void run(@Nullable Boolean data) {
				if(callback == null) return;

				if(callback1 != null && callback1.getObject() != null && (Boolean) callback1.getObject())
					callback.run();

				callback2.setObject(data);
			}
		};

		loadDays(day, 14, forceSync, callback1);

		day.add(Calendar.DAY_OF_MONTH, -7);
		loadDays(day, 7, forceSync, callback2);
	}

	private void loadDays(Calendar temp, int daysToLoad, boolean[] forceSync, DataClassCallback<Boolean> callback) {
		List<Pair<Calendar, Integer>> daysToDownload = new ArrayList<>();

		Calendar day = (Calendar)temp.clone();
		for(int i = 0, j; i < daysToLoad; i++, day.add(Calendar.DAY_OF_MONTH, 1)) {
			if(isLoaded(day, forceSync)) continue;

			if(prefs.isTimetableForDay(day) && isNotForced(forceSync) && user == null) {
				j = 0;
				while(prefs.isTimetableForDay(day) && !isLoaded(day, forceSync)) {
					loadedDays.put(AppPreferences.dateFormat.format(day.getTime()), true);
					j++;
					day.add(Calendar.DAY_OF_MONTH, 1);
				}
				setTimetable(prefs.getTimetableForDays(day, j + 1, -(j + 1)), true);
				i += j;
			} else {
				j = 0;
				while(!prefs.isTimetableForDay(day) && j < 7 && isLoaded(day, forceSync)) {
					loadedDays.put(AppPreferences.dateFormat.format(day.getTime()), true);
					j++;
					day.add(Calendar.DAY_OF_MONTH, 1);
				}
				daysToDownload.add(Pair.create((Calendar) day.clone(), j + 1));
				i += j;
			}

			loadedDays.put(AppPreferences.dateFormat.format(day.getTime()), true);
		}

		new GetUserScheduleAsync(user != null ? user.id : null, daysToDownload, courseList -> {
			setTimetable(courseList, true);
			callback.run(true);
		}).execute(rootView.getContext());
	}

	private boolean isLoaded(Calendar day, boolean... forceSync) {
		return loadedDays.containsKey(AppPreferences.dateFormat.format(day.getTime())) && isNotForced(forceSync);
	}

	private boolean isNotForced(boolean[] forceSync) {
		return forceSync == null || forceSync.length <= 0 || !forceSync[0];
	}

	private void setTimetable(List<Course> courseList, boolean save) {
		if(save) allCourses.addAll(courseList);

		ArrayList<String> ids1 = new ArrayList<>();
		ArrayList<String> ids2 = new ArrayList<>();
		ArrayList<String> ids3 = new ArrayList<>();
		for(Filter filter : courses) if(!filter.isChecked()) ids1.add(filter.getId());
		for(Filter filter : classTypes) if(!filter.isChecked()) ids2.add(filter.getId());
		for(Filter filter : buildings) if(!filter.isChecked()) ids3.add(filter.getId());

		for(Course c : courseList) {
			if(c.start_time == null) {
				showLogoutRequestMessage();
				return;
			}
			List<WeekViewEvent> courses = allEvents.get(new SimpleDateFormat("MM, yyyy", Locale.getDefault()).format(c.start_time));
			if(courses == null) courses = new ArrayList<>();
			WeekViewEvent event = Course.toWeekViewEvent(prefs, c);
			if(!Course.containsWeekViewEvent(courses, event) &&
					!(ids1.contains(c.courseId) || ids2.contains(c.classtypeId) || ids3.contains(c.buildingId)))
				courses.add(event);
			allEvents.put(new SimpleDateFormat("MM, yyyy", Locale.getDefault()).format(c.start_time), courses);
		}
		weekView.refreshDataset();
	}

	private void showLogoutRequestMessage() {
		FragmentActivity activity = getActivity();
		if(activity == null) return;

		Utils.makeDialog(activity, R.layout.dialog_show_message, dialog -> {
			if(dialog == null) return;

			((AppCompatTextView) dialog.findViewById(R.id.title)).setText(getResources().getString(R.string.message));
			((AppCompatTextView) dialog.findViewById(R.id.message)).setText(getResources().getString(R.string.logout_request));

			dialog.findViewById(R.id.okButton).setOnClickListener(v -> {
				dialog.dismiss();
				prefs.edit().clear().commit();
				activity.startActivity(new Intent(activity, MainActivity.class));
				activity.finish();
			});
		}, Utils.UNDEFINED_WINDOW_SIZE, ViewGroup.LayoutParams.WRAP_CONTENT);
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public ArrayList<Filter> getClassTypes() {
		return classTypes;
	}

	public void setForceRefresh() {
		this.forceRefresh = true;
	}
}
