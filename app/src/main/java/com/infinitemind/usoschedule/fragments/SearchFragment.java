package com.infinitemind.usoschedule.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;

import com.infinitemind.usoschedule.R;
import com.infinitemind.usoschedule.adapter.UserListAdapter;
import com.infinitemind.usoschedule.async.GetSearchResultAsync;
import com.infinitemind.usoschedule.clickListeners.OnUserClickListener;
import com.infinitemind.usoschedule.model.User;
import com.infinitemind.usoschedule.runnable.DataClassCallback;
import com.infinitemind.usoschedule.runnable.OnTextChanged;
import com.infinitemind.usoschedule.runnable.Timer;
import com.infinitemind.usoschedule.utils.AppPreferences;
import com.infinitemind.usoschedule.utils.BottomPaddingItemDecoration;
import com.infinitemind.usoschedule.utils.UnScrollableLayoutManager;
import com.infinitemind.usoschedule.utils.Utils;
import com.tapadoo.alerter.Alerter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SearchFragment extends Fragment {

	private UserListAdapter recentlySearchedListAdapter;
	private GetSearchResultAsync searchResultAsync;
	private OnUserClickListener clickListener;
	private UserListAdapter userListAdapter;
	private ArrayList<User> recent;
	private ArrayList<User> recentlySearched;
	private ArrayList<User> searchResult;
	private ViewGroup rootView;
	private CountDownTimer timer;
	private AppPreferences appPreferences;
	private boolean forceRefresh;

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		if(rootView == null || forceRefresh) {
			rootView = (ViewGroup)inflater.inflate(R.layout.fragment_search, container, false);

			appPreferences = new AppPreferences(rootView.getContext());

			setRecentlySearchedList();
			setSearchList();
			setEditSearchListener();

			toggleTitles();
		}

		if(forceRefresh = Utils.isOffline(rootView.getContext())) {
			Typeface arconFont = ResourcesCompat.getFont(rootView.getContext(), R.font.arcon);
			FragmentActivity activity = getActivity();
			if(arconFont != null && activity != null) {
				Alerter.create(activity)
						.setBackgroundColorRes(R.color.colorPrimary)
						.setTitle(getResources().getString(R.string.message))
						.setText(getResources().getString(R.string.connection_warning))
						.enableSwipeToDismiss()
						.setTitleTypeface(arconFont)
						.setTextTypeface(arconFont)
						.setDuration(3000)
						.show();
			}
		}
		return rootView;
	}

	private void setRecentlySearchedList() {
		RecyclerView recentlySearchedList = rootView.findViewById(R.id.recentlySearchedList);
		recentlySearchedList.setLayoutManager(new UnScrollableLayoutManager(rootView.getContext(), LinearLayoutManager.VERTICAL, false));
		recentlySearchedList.setAdapter(recentlySearchedListAdapter = new UserListAdapter(recentlySearched = getRecentlySearched(), false));
		recentlySearchedList.setNestedScrollingEnabled(false);
		recentlySearchedListAdapter.setOnClickListener(new UserListAdapter.MyClickListener() {
			@Override public void onClick(int position) {
				clickListener.onUserClick(recentlySearched.get(position));
				SearchFragment.this.getRecentlySearched();
			}

			@Override public boolean onLongClick(int position) {
				Utils.makeDialog(getActivity(), R.layout.dialog_show_message, dialog -> {
					if(dialog != null) {
						((AppCompatTextView)dialog.findViewById(R.id.title)).setText(getResources().getString(R.string.text_sure));
						((AppCompatTextView)dialog.findViewById(R.id.message)).setText(getResources().getString(R.string.text_delete));

						dialog.findViewById(R.id.okButton).setOnClickListener(view -> {
							recentlySearched.remove(position);
							appPreferences.edit().setRecentlySearched(searchResult);
							getRecentlySearched();
							recentlySearchedListAdapter.notifyDataSetChanged();
							toggleTitles();
							dialog.dismiss();
						});
					}
				}, Utils.UNDEFINED_WINDOW_SIZE, ViewGroup.LayoutParams.WRAP_CONTENT);
				return false;
			}
		});
	}

	private void setSearchList() {
		RecyclerView searchResultList = rootView.findViewById(R.id.searchResultList);
		searchResultList.setLayoutManager(new UnScrollableLayoutManager(rootView.getContext(), LinearLayoutManager.VERTICAL, false));
		searchResultList.setAdapter(userListAdapter = new UserListAdapter(searchResult = new ArrayList<>(), false));
		searchResultList.addItemDecoration(new BottomPaddingItemDecoration(Utils.dpToPx(rootView.getContext(), 100)));
		searchResultList.setNestedScrollingEnabled(false);
		userListAdapter.setOnClickListener(position -> {
			clickListener.onUserClick(searchResult.get(position));
			boolean contains = false;
			for(User u : recent)
				if(u.id.equals(searchResult.get(position).id)) {
					contains = true;
					break;
				}
			if(!contains) appPreferences.edit().addRecentlySearched(searchResult.get(position));
			getRecentlySearched();
			recentlySearchedListAdapter.notifyDataSetChanged();
			toggleTitles();
		});
	}

	private void setEditSearchListener() {
		AppCompatEditText editText = rootView.findViewById(R.id.editSearch);

		if(editText != null) {
			editText.setOnEditorActionListener((textView, actionId, event) -> {
				if(actionId == EditorInfo.IME_ACTION_SEARCH || event.getAction() == KeyEvent.ACTION_DOWN && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
					timer.cancel();
					displayResult(textView.getText().toString());
					return true;
				}
				return false;
			});
			editText.addTextChangedListener(new OnTextChanged(editable -> {
				if(editable != null) {
					rootView.findViewById(R.id.buttonErase).setVisibility(editable.toString().isEmpty() ? View.GONE : View.VISIBLE);
					recentlySearched.clear();
					recentlySearched.addAll(recent);
					for(int i = recentlySearched.size() - 1; i >= 0; i--) {
						User user = recentlySearched.get(i);
						if(!user.getUserName().contains(editable.toString()))
							recentlySearched.remove(user);
					}
					recentlySearchedListAdapter.notifyDataSetChanged();

					toggleTitles();

					timer = new Timer(3000, () -> {
						if(editable.toString().isEmpty())
							eraseResult();
						else displayResult(editable.toString());
					}).start();
				}
			}));
		}
		rootView.findViewById(R.id.buttonErase).setOnClickListener(view -> eraseResult());
	}

	private void eraseResult() {
		Editable text = ((AppCompatEditText)rootView.findViewById(R.id.editSearch)).getText();
		if(text != null) text.clear();
		searchResult.clear();
		userListAdapter.notifyDataSetChanged();

		toggleTitles();
	}

	private void displayResult(String query) {
		rootView.findViewById(R.id.loadingIndicator).setVisibility(View.VISIBLE);
		
		if(searchResultAsync != null && !searchResultAsync.isCancelled())
			searchResultAsync.cancel(true);
		getSearchResult(query, 0);

		toggleTitles();
	}

	private void toggleTitles() {
		rootView.findViewById(R.id.recentlySearched).setVisibility(recentlySearched.isEmpty() ? View.GONE : View.VISIBLE);
		rootView.findViewById(R.id.searchResult).setVisibility(searchResult == null || searchResult.isEmpty() || recentlySearched.isEmpty() ? View.GONE : View.VISIBLE);
		rootView.findViewById(R.id.explanationText).setVisibility(searchResult == null || (searchResult.isEmpty() && recentlySearched.isEmpty()) ? View.VISIBLE : View.GONE);
	}

	private void getSearchResult(String query, int start) {
		searchResultAsync = new GetSearchResultAsync(query, start, new DataClassCallback<List<User>>() {
			@Override
			public void run(@Nullable List<User> users) {
				if(users != null) {
					searchResult.addAll(users);
					filterDuplicates(searchResult, query);
					Collections.sort(searchResult, Utils.userComparator);
					userListAdapter.notifyDataSetChanged();
					if((boolean)getObject()) getSearchResult(query, start + users.size());

					toggleTitles();
					rootView.findViewById(R.id.loadingIndicator).setVisibility(View.GONE);
				}
			}
		});
		searchResultAsync.execute(rootView.getContext());
	}

	private void filterDuplicates(List<User> users, String query) {
		for(int i = users.size() - 1; i >= 0; i--) {
			if(!users.get(i).getUserName().contains(query)) {
				users.remove(i);
				continue;
			}
			for(int j = i - 1; j >= 0; j--)
				if(users.get(i).id.equals(users.get(j).id)) {
					users.remove(i);
					break;
				}
		}
	}

	private ArrayList<User> getRecentlySearched() {
		ArrayList<User> list = appPreferences.getRecentlySearched();
		if(recent == null) recent = new ArrayList<>();
		else recent.clear();
		recent.addAll(list);
		return list;
	}

	public SearchFragment setOnUserClickListener(OnUserClickListener runnable) {
		this.clickListener = runnable;
		return this;
	}
}
