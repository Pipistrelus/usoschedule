package com.infinitemind.usoschedule.fragments;

import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.Space;

import com.infinitemind.usoschedule.BuildConfig;
import com.infinitemind.usoschedule.R;
import com.infinitemind.usoschedule.activities.MainActivity;
import com.infinitemind.usoschedule.activities.MessageActivity;
import com.infinitemind.usoschedule.model.Filter;
import com.infinitemind.usoschedule.runnable.DataCallback;
import com.infinitemind.usoschedule.runnable.OnSeekBarChanged;
import com.infinitemind.usoschedule.settings.HeaderItem;
import com.infinitemind.usoschedule.settings.ImageItem;
import com.infinitemind.usoschedule.settings.SettingsItem;
import com.infinitemind.usoschedule.settings.SimpleItem;
import com.infinitemind.usoschedule.utils.AppPreferences;
import com.infinitemind.usoschedule.utils.Utils;
import com.infinitemind.usoschedule.views.CustomFlagView;
import com.infinitemind.usoschedule.views.WeekView;
import com.skydoves.colorpickerpreference.ColorPickerView;

import java.util.ArrayList;
import java.util.Collections;

public class SettingsFragment extends Fragment {

	@Nullable private ArrayList<Filter> classTypes;
	private int currentValue;
	private ViewGroup rootView;

	private ImageItem[] eventColorItems;
	private SettingsItem defaultVisibleDaysItem;
	private SettingsItem eventTextSizeItem;
	private SettingsItem breakLengthItem;
	private SettingsItem widgetFirstDayItem;
	private SettingsItem widgetLastDayItem;
	private SettingsItem nightThemeItem;
	private SettingsItem versionItem;
	private SettingsItem termsConditionsItem;
	private SettingsItem aboutMeItem;
	private SettingsItem logoutItem;

	private SettingsItem appearanceHeaderItem;
	private SettingsItem widgetHeaderItem;
	private SettingsItem timetableHeaderItem;
	private SettingsItem aboutHeaderItem;
	private SettingsItem otherHeaderItem;

	private AppPreferences appPreferences;
	private ViewGroup settingsContainer;

	@Nullable @Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		if(rootView == null) {
			rootView = (ViewGroup)inflater.inflate(R.layout.fragment_settings, container, false);

			appPreferences = new AppPreferences(rootView.getContext());
			settingsContainer = rootView.findViewById(R.id.settingsContainer);

			setSettingsList();
		}

		setClassTypesSettings();

		return rootView;
	}

	private void setSettingsList() {
//		HEADERS

		timetableHeaderItem = new HeaderItem(rootView.getContext())
				.setTitle(getResources().getString(R.string.timetable))
				.refresh();

		widgetHeaderItem = new HeaderItem(rootView.getContext())
				.setTitle(getResources().getString(R.string.widget))
				.refresh();

		appearanceHeaderItem = new HeaderItem(rootView.getContext())
				.setTitle(getResources().getString(R.string.appearance))
				.refresh();

		aboutHeaderItem = new HeaderItem(rootView.getContext())
				.setTitle(getResources().getString(R.string.about))
				.refresh();

		otherHeaderItem = new HeaderItem(rootView.getContext())
				.setTitle(getResources().getString(R.string.other))
				.refresh();

		settingsContainer.addView(timetableHeaderItem);

//		ITEMS

		eventTextSizeItem = new SimpleItem(rootView.getContext())
				.setOnClickListener((DataCallback<Void>)data -> showChooseValueDialog(12, 24, appPreferences.getEventTextSize(), getResources().getString(R.string.event_text_size), () -> {
					eventTextSizeItem.setSubtitle(String.valueOf(currentValue)).refreshSubtitle();
					appPreferences.edit().setEventTextSize(currentValue);
				}))
				.setTitle(getResources().getString(R.string.event_text_size)).setSubtitle(String.valueOf(appPreferences.getEventTextSize()))
				.refresh();

		defaultVisibleDaysItem = new SimpleItem(rootView.getContext())
				.setOnClickListener((DataCallback<Void>)data -> showChooseValueDialog(WeekView.MIN_NUMBER_OF_VISIBLE_DAYS, WeekView.MAX_NUMBER_OF_VISIBLE_DAYS,
						appPreferences.getDefaultVisibleDays(), getResources().getString(R.string.default_visible_days), () -> {
							defaultVisibleDaysItem.setSubtitle(String.valueOf(currentValue)).refreshSubtitle();
							appPreferences.edit().setDefaultVisibleDays(currentValue);
						}))
				.setTitle(getResources().getString(R.string.default_visible_days)).setSubtitle(String.valueOf(appPreferences.getDefaultVisibleDays()))
				.refresh();

		breakLengthItem = new SimpleItem(rootView.getContext())
				.setOnClickListener((DataCallback<Void>)data -> showChooseValueDialog(0, 30, appPreferences.getBreakLength(), getResources().getString(R.string.length_of_breaks), () -> {
					breakLengthItem.setSubtitle(String.valueOf(currentValue)).refreshSubtitle();
					appPreferences.edit().setBreakLength(currentValue);
				}))
				.setTitle(getResources().getString(R.string.length_of_breaks)).setSubtitle(String.valueOf(appPreferences.getBreakLength()))
				.refresh()
				.hideDivider();

		widgetFirstDayItem = new SimpleItem(rootView.getContext())
				.setOnClickListener((DataCallback<Void>)data -> showChooseValueDialog(getWeekDays(), appPreferences.getWidgetFirstDay(), getResources().getString(R.string.first_day), () -> {
					widgetFirstDayItem.setSubtitle(getWeekDays()[currentValue]).refreshSubtitle();
					appPreferences.edit().setWidgetFirstDay(currentValue);

					Activity activity = getActivity();
					if(activity != null)
						activity.sendBroadcast(new Intent(AppWidgetManager.ACTION_APPWIDGET_UPDATE));
				}))
				.setTitle(getResources().getString(R.string.first_day)).setSubtitle(getWeekDays()[appPreferences.getWidgetFirstDay()])
				.refresh();

		widgetLastDayItem = new SimpleItem(rootView.getContext())
				.setOnClickListener((DataCallback<Void>)data -> showChooseValueDialog(getWeekDays(), appPreferences.getWidgetLastDay(), getResources().getString(R.string.last_day), () -> {
					widgetLastDayItem.setSubtitle(getWeekDays()[currentValue]).refreshSubtitle();
					appPreferences.edit().setWidgetLastDay(currentValue);

					Activity activity = getActivity();
					if(activity != null)
						activity.sendBroadcast(new Intent(AppWidgetManager.ACTION_APPWIDGET_UPDATE));
				}))
				.setTitle(getResources().getString(R.string.last_day)).setSubtitle(getWeekDays()[appPreferences.getWidgetLastDay()])
				.refresh()
				.hideDivider();

		versionItem = new SimpleItem(rootView.getContext())
				.toggle(false)
				.setTitle(getResources().getString(R.string.version)).setSubtitle(getVersion())
				.refresh();

		termsConditionsItem = new SimpleItem(rootView.getContext())
				.setOnClickListener((DataCallback<Void>)data -> showMessageActivity(getTermsAndConditions()))
				.setTitle(getResources().getString(R.string.terms_conditions)).setSubtitle(getResources().getString(R.string.stuff_you_need_know))
				.refresh();

		aboutMeItem = new SimpleItem(rootView.getContext())
				.setOnClickListener((DataCallback<Void>)data -> showMessageActivity(getAboutMe()))
				.setTitle(getResources().getString(R.string.about_me)).setSubtitle(getResources().getString(R.string.stuff_you_might_want_know))
				.refresh()
				.hideDivider();

		nightThemeItem = new SimpleItem(rootView.getContext())
				.setOnClickListener((DataCallback<Void>)data -> {
					appPreferences.edit().setNightModeEnabled(!appPreferences.isNightModeEnabled());

					Activity activity = getActivity();
					if(activity == null) return;

					activity.sendBroadcast(new Intent(AppWidgetManager.ACTION_APPWIDGET_UPDATE));

					activity.startActivity(new Intent(getContext(), MainActivity.class));
					activity.finish();
					activity.overridePendingTransition(0, 0);
				})
				.setTitle(getResources().getString(R.string.toggle_night_mode)).setSubtitle(getResources().getString(R.string.requires_restart))
				.refresh()
				.hideDivider();

		logoutItem = new SimpleItem(rootView.getContext())
				.setOnClickListener((DataCallback<Void>)data -> showConfirmationDialog(() -> {
					Activity activity = getActivity();
					if(activity == null) return;

					appPreferences.edit().clear().commit();
					activity.startActivity(new Intent(activity, MainActivity.class));
					activity.finish();
				}))
				.setTitle(getResources().getString(R.string.logout)).setSubtitle(getResources().getString(R.string.tap_to_logout))
				.refresh()
				.hideDivider();

		settingsContainer.addView(defaultVisibleDaysItem);
		settingsContainer.addView(eventTextSizeItem);
		settingsContainer.addView(breakLengthItem);

		settingsContainer.addView(widgetHeaderItem);
		settingsContainer.addView(widgetFirstDayItem);
		settingsContainer.addView(widgetLastDayItem);

		settingsContainer.addView(appearanceHeaderItem);
		settingsContainer.addView(nightThemeItem);

		settingsContainer.addView(aboutHeaderItem);
		settingsContainer.addView(versionItem);
		settingsContainer.addView(termsConditionsItem);
		settingsContainer.addView(aboutMeItem);

		settingsContainer.addView(otherHeaderItem);
		settingsContainer.addView(logoutItem);

		Space space = new Space(rootView.getContext());
		space.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, Utils.dpToPx(rootView.getContext(), 100)));
		settingsContainer.addView(space);
	}

	private String[] getWeekDays() {
		return new String[]{"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
	}

	private String getAboutMe() {
		return Utils.getAssetFile(rootView.getContext(), "about_me.html");
	}

	private String getTermsAndConditions() {
		return Utils.getAssetFile(rootView.getContext(), "terms_and_conditions.html");
	}

	private String getVersion() {
		return BuildConfig.VERSION_NAME.concat(".").concat(String.valueOf(BuildConfig.VERSION_CODE));
	}

	private void setClassTypesSettings() {
		if(classTypes != null) {
			if(eventColorItems != null)
				settingsContainer.removeViewsInLayout(1, eventColorItems.length);

			Collections.sort(classTypes, (c1, c2) -> c2.getValue().compareTo(c1.getValue()));
			eventColorItems = new ImageItem[classTypes.size()];
			for(int i = 0; i < classTypes.size(); i++) {
				int finalI = i;

				eventColorItems[i] = new ImageItem(rootView.getContext());
				eventColorItems[i].setImageResource(R.drawable.ic_dot)
						.setImageColor(classTypes.get(i).getColor())
						.setOnClickListener((DataCallback<Void>)data -> showChooseColorDialog(color -> {
							if(color == null) return;
							eventColorItems[finalI].setImageColor(color).refresh();
							appPreferences.edit().setEventColorForClassType(classTypes.get(finalI).getValue(), color);
						}))
						.setTitle(getResources().getString(R.string.color_of, classTypes.get(i).getValue())).setSubtitle(getResources().getString(R.string.tap_to_change))
						.refresh();

				settingsContainer.addView(eventColorItems[i], 1);
			}
		}
	}

	private void showMessageActivity(String message) {
		startActivity(new Intent(getActivity(), MessageActivity.class).putExtra(MessageActivity.MESSAGE_TAG, message));
	}

	private void showChooseColorDialog(DataCallback<Integer> callback) {
		Utils.makeDialog(getActivity(), R.layout.dialog_choose_color, dialog -> {
			if(dialog == null) return;

			ColorPickerView colorPicker = dialog.findViewById(R.id.colorPicker);
			colorPicker.setFlagView(new CustomFlagView(getContext()));

			dialog.findViewById(R.id.okButton).setOnClickListener(v -> {
				callback.run(colorPicker.getColor());
				dialog.dismiss();
			});

		}, Utils.UNDEFINED_WINDOW_SIZE, ViewGroup.LayoutParams.WRAP_CONTENT);
	}

	private void showChooseValueDialog(int min, int max, int current, String title, Runnable callback) {
		Integer[] tab = new Integer[max - min + 1];
		for(int i = min; i <= max; i++) tab[i - min] = i;
		showChooseValueDialog(tab, current - min, title, callback);
	}

	private <T> void showChooseValueDialog(T[] values, int current, String title, Runnable callback) {
		Utils.makeDialog(getActivity(), R.layout.dialog_choose_value, dialog -> {
			int min = 0;
			int max = values.length - 1;

			if(dialog == null) return;

			((AppCompatTextView)dialog.findViewById(R.id.title)).setText(title);

			SeekBar seekBar = dialog.findViewById(R.id.seekBar);
			seekBar.setOnSeekBarChangeListener(new OnSeekBarChanged(progress -> {
				currentValue = progress != null ? progress + min : currentValue;
				((AppCompatTextView)dialog.findViewById(R.id.message)).setText(getResources().getString(R.string.current_value, values[currentValue]));
			}));
			seekBar.setMax(max - min);
			seekBar.setProgress(current - min);

			dialog.findViewById(R.id.okButton).setOnClickListener(v -> {
				callback.run();
				dialog.dismiss();
			});

		}, Utils.UNDEFINED_WINDOW_SIZE, ViewGroup.LayoutParams.WRAP_CONTENT);
	}

	private void showConfirmationDialog(Runnable callback) {
		Utils.makeDialog(getActivity(), R.layout.dialog_show_message, dialog -> {
			if(dialog == null) return;

			((AppCompatTextView)dialog.findViewById(R.id.message)).setText(getResources().getString(R.string.text_logout));

			dialog.findViewById(R.id.okButton).setOnClickListener(v -> {
				callback.run();
				dialog.dismiss();
			});
		}, Utils.UNDEFINED_WINDOW_SIZE, ViewGroup.LayoutParams.WRAP_CONTENT);
	}

	public SettingsFragment setClassTypes(@Nullable ArrayList<Filter> classTypes) {
		this.classTypes = classTypes;
		return this;
	}
}
