package com.infinitemind.usoschedule.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v4.content.res.ResourcesCompat;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.text.TextUtils;

import com.infinitemind.usoschedule.R;
import com.infinitemind.usoschedule.model.Course;
import com.infinitemind.usoschedule.utils.AppPreferences;
import com.infinitemind.usoschedule.utils.Utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

public class WeekViewStatic {

	public static void draw(Data data, Canvas canvas) {
		drawDaysColors(data, canvas);
		drawDayHeader(data, canvas);
		drawHourHeader(data, canvas);
		drawHorizontalLines(data, canvas);
		drawEvents(data, canvas);
	}

	private static void drawDayHeader(Data data, Canvas canvas) {
		canvas.drawRect(0, 0, data.WIDTH, data.headerHeight, data.headerPaint);

		canvas.save();
		canvas.clipRect(data.headerWidth, 0, data.WIDTH, data.headerHeight);

		Calendar today = Calendar.getInstance();
		Calendar currentDay = getFirstDay(data, today);
		for(int i = 0; i < data.numberOfVisibleDays; i++, currentDay.add(Calendar.DAY_OF_MONTH, 1)) {
			String day = data.dateTimeInterpreter.interpretDate(currentDay);

			float x = i * data.dayWidth + (data.dayWidth - data.headerTextPaint.measureText(day)) / 2 + data.headerWidth;

			if(WeekView.WeekViewUtil.isSameDay(currentDay, today))
				data.headerTextPaint.setColor(data.todayTextColor);
			else data.headerTextPaint.setColor(data.headerTextColor);

			canvas.drawText(day, x, data.headerPadding.top + data.headerTextSize, data.headerTextPaint);
			data.headerTextPaint.setColor(data.headerTextColor);
		}
		canvas.restore();
	}

	private static void drawHourHeader(Data data, Canvas canvas) {
		canvas.drawRect(0, 0, data.headerWidth, data.HEIGHT, data.headerPaint);

		canvas.save();
		canvas.clipRect(0, data.headerHeight, data.headerWidth, data.HEIGHT);

		for(int i = data.startHour, index = 0; i < data.endHour; i++, index++) {
			String hour = data.dateTimeInterpreter.interpretTime(i);

			float y = data.headerPadding.top + data.hourHeight * index + data.headerHeight;
			if(y - data.headerTextSize < 0 || y - data.headerTextSize > data.HEIGHT) continue;

			canvas.drawText(hour, data.headerPadding.left, y, data.headerTextPaint);
		}
		canvas.restore();
	}

	private static void drawEvents(Data data, Canvas canvas) {
		canvas.save();
		canvas.clipRect(data.headerWidth, data.headerHeight, data.WIDTH, data.HEIGHT);
		Calendar currentDay = getFirstDay(data, Calendar.getInstance());
		for(int i = 0; i < data.numberOfVisibleDays; i++, currentDay.add(Calendar.DAY_OF_MONTH, 1)) {
			float x = i * data.dayWidth + data.headerWidth;
			drawDayEvents(data, canvas, currentDay, x);
		}
		canvas.restore();
	}

	private static void drawDayEvents(Data data, Canvas canvas, Calendar day, float x) {
		float maxHeight = data.hourHeight * 24;
		for(WeekView.EventRect eventRect : data.eventRects)
			if(WeekView.WeekViewUtil.isSameDay(eventRect.event.getStartTime(), day)) {
				float startFraction = WeekView.WeekViewUtil.getFractionOfDay(eventRect.event.getStartTime());
				float startRealY = startFraction * maxHeight - data.startHour * data.hourHeight;
				float endFraction = WeekView.WeekViewUtil.getFractionOfDay(eventRect.event.getEndTime());
				float endRealY = endFraction * maxHeight - data.startHour * data.hourHeight;

				float left = x + data.dayWidth * eventRect.left + data.overlappingEventsGap;
				float top = startRealY + data.headerHeight + data.headerPadding.top;
				float right = left + data.dayWidth * eventRect.width - data.overlappingEventsGap;
				float bottom = endRealY + data.headerHeight + data.headerPadding.top;

				eventRect.rectF = new RectF(left, top, right, bottom);
				data.eventPaint.setColor(eventRect.event.getColor() == 0 ? data.defaultEventColor : eventRect.event.getColor());
				canvas.drawRoundRect(eventRect.rectF, data.eventCornerRadius, data.eventCornerRadius, data.eventPaint);
				drawEventTitle(data, eventRect.event, eventRect.rectF, canvas, eventRect.rectF.top, eventRect.rectF.left);
			}
	}

	private static void drawEventTitle(Data data, WeekView.WeekViewEvent event, RectF rect, Canvas canvas, float originalTop, float originalLeft) {
		int availableHeight = (int)(rect.bottom - originalTop - data.eventTextPadding.left - data.eventTextPadding.right);
		int availableWidth = (int)(rect.right - originalLeft - data.eventTextPadding.top - data.eventTextPadding.bottom);

		if(availableWidth <= 0 || availableHeight <= 0) return;

		// Get text dimensions.
		StaticLayout textLayout = new StaticLayout(event.getName(), data.eventTextPaint, availableWidth, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
			textLayout = StaticLayout.Builder.obtain(event.getName(), 0, event.getName().length(), data.eventTextPaint, availableWidth).build();

		int lineHeight = textLayout.getHeight() / textLayout.getLineCount();

		if(availableHeight >= lineHeight) {
			// Calculate available number of line counts.
			int availableLineCount = availableHeight / lineHeight;
			do {
				// Ellipsize text to fit into event rect.
				textLayout = new StaticLayout(TextUtils.ellipsize(event.getName(), data.eventTextPaint, availableLineCount * availableWidth, TextUtils.TruncateAt.END),
						data.eventTextPaint, (int)(rect.right - originalLeft - data.eventTextPadding.left - data.eventTextPadding.right), Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);

				// Reduce line count.
				availableLineCount--;

				// Repeat until text is short enough.
			} while(textLayout.getHeight() > availableHeight);

			// Draw text.
			canvas.save();
			canvas.translate(originalLeft + data.eventTextPadding.left, originalTop + data.eventTextPadding.top);
			textLayout.draw(canvas);
			canvas.restore();
		}
	}

	private static void drawDaysColors(Data data, Canvas canvas) {
		Calendar today = Calendar.getInstance();
		Calendar currentDay = getFirstDay(data, today);
		for(int i = 0; i < data.numberOfVisibleDays; i++, currentDay.add(Calendar.DAY_OF_MONTH, 1)) {
			int backgroundColor = WeekView.WeekViewUtil.isWeekday(currentDay) ? data.weekendBackgroundColor : data.futureBackgroundColor;

			if(WeekView.WeekViewUtil.isSameDay(currentDay, today)) {
				float x = i * data.dayWidth + data.headerWidth;

				data.backgroundPaint.setColor(data.todayBackgroundColor);
				canvas.drawRect(x, 0, x + data.dayWidth, data.HEIGHT, data.backgroundPaint);
			} else {
				if(currentDay.before(today))
					data.backgroundPaint.setColor(data.pastBackgroundColor);
				else data.backgroundPaint.setColor(backgroundColor);

				float x = i * data.dayWidth + data.headerWidth;

				canvas.drawRect(x, 0, x + data.dayWidth, data.HEIGHT, data.backgroundPaint);
			}
		}
	}

	private static void drawHorizontalLines(Data data, Canvas canvas) {
		for(int i = data.startHour, index = 0; i < data.endHour; i++, index++) {

//			Check if it's visible on the screen
			float y = data.headerPadding.top + data.hourHeight * index + data.headerHeight;
			if(y - data.headerTextSize < 0 || y - data.headerTextSize > data.HEIGHT) continue;

			canvas.drawLine(data.headerWidth, y, data.WIDTH, y, data.horizontalLinePaint);
		}
	}

	public static void init(Data data, Context context) {
		data.dateTimeInterpreter = new WeekView.DateTimeInterpreter() {
			@Override public String interpretDate(Calendar date) {
				return new SimpleDateFormat("dd.MM, EEE", Locale.getDefault()).format(date.getTime());
			}

			@Override public String interpretTime(int hour) {
				return (hour < 10 ? "0" : "") + hour + ":00";
			}
		};

		data.headerTextColor = data.headerTextColor != 0 ? data.headerTextColor : context.getResources().getColor(R.color.headerTextColor);
		data.headerColor = data.headerColor != 0 ? data.headerColor : context.getResources().getColor(R.color.headerColor);
		data.todayTextColor = data.todayTextColor != 0 ? data.todayTextColor : context.getResources().getColor(R.color.todayTextColor);
		data.defaultEventColor = data.defaultEventColor != 0 ? data.defaultEventColor : context.getResources().getColor(R.color.defaultEventColor);
		data.futureBackgroundColor = data.futureBackgroundColor != 0 ? data.futureBackgroundColor : context.getResources().getColor(R.color.futureBackgroundColor);
		data.pastBackgroundColor = data.pastBackgroundColor != 0 ? data.pastBackgroundColor : context.getResources().getColor(R.color.pastBackgroundColor);
		data.weekendBackgroundColor = data.weekendBackgroundColor != 0 ? data.weekendBackgroundColor : context.getResources().getColor(R.color.weekendBackgroundColor);
		data.todayBackgroundColor = data.todayBackgroundColor != 0 ? data.todayBackgroundColor : context.getResources().getColor(R.color.colorSemiPrimary);
		data.horizontalLineColor = data.horizontalLineColor != 0 ? data.horizontalLineColor : context.getResources().getColor(R.color.horizontalLineColor);
		data.headerTextSize = data.headerTextSize != 0 ? data.headerTextSize : context.getResources().getDimension(R.dimen.default_header_text_size) * 0.75f;
		data.eventTextSize = data.eventTextSize != 0 ? data.eventTextSize : context.getResources().getDimension(R.dimen.default_event_text_size) * 0.5f;
		data.eventTextColor = data.eventTextColor != 0 ? data.eventTextColor : context.getResources().getColor(R.color.defaultEventTextColor);

		data.headerPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		data.headerPaint.setColor(data.headerColor);

		data.headerTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		data.headerTextPaint.setColor(data.headerTextColor);
		data.headerTextPaint.setTextSize(data.headerTextSize);
		data.headerTextPaint.setTypeface(ResourcesCompat.getFont(context, R.font.arcon));

		data.horizontalLinePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		data.horizontalLinePaint.setColor(data.horizontalLineColor);

		data.backgroundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		data.backgroundPaint.setColor(data.futureBackgroundColor);

		data.eventPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		data.eventPaint.setColor(data.defaultEventColor);

		data.eventTextPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG);
		data.eventTextPaint.setColor(data.eventTextColor);
		data.eventTextPaint.setTypeface(Typeface.create(ResourcesCompat.getFont(context, R.font.arcon), Typeface.BOLD));
		data.eventTextPaint.setTextSize(data.eventTextSize);

		int dp5 = Utils.dpToPx(context, 5);
		data.headerPadding = new WeekView.Padding();
		data.headerPadding.top = 2 * dp5;
		data.headerPadding.bottom = 2 * dp5;
		data.headerPadding.left = dp5;
		data.headerPadding.right = dp5;

		data.eventTextPadding = new WeekView.Padding();
		data.eventTextPadding.left = data.eventTextPadding.right = data.eventTextPadding.top = data.eventTextPadding.bottom = dp5 / 2f;

		Rect textBounds = new Rect();
		String time = data.dateTimeInterpreter.interpretTime(0);
		data.headerTextPaint.getTextBounds(time, 0, time.length(), textBounds);
		data.headerWidth = textBounds.width() + data.headerPadding.left + data.headerPadding.right;
		data.headerHeight = textBounds.height() + data.headerPadding.top + data.headerPadding.bottom;

		data.numberOfVisibleDays = data.lastDay - data.firstDay + (data.lastDay - data.firstDay < 0 ? 8 : 1);

		data.courses = new AppPreferences(context).getTimetableForDays(
				getFirstDay(data, Calendar.getInstance()), data.numberOfVisibleDays, 0);

		Calendar temp = Calendar.getInstance();
		for(Course c : data.courses) {
			temp.setTimeInMillis(c.start_time.getTime());
			temp.add(Calendar.MINUTE, -30);
			if(temp.get(Calendar.HOUR_OF_DAY) < data.startHour)
				data.startHour = temp.get(Calendar.HOUR_OF_DAY);

			temp.setTimeInMillis(c.end_time.getTime());
			temp.add(Calendar.MINUTE, 90);
			if(temp.get(Calendar.HOUR_OF_DAY) > data.endHour)
				data.endHour = temp.get(Calendar.HOUR_OF_DAY);
		}

		calculateDayWidth(data);
		calculateHourHeight(data);

		computeEventsRects(data, context);
	}

	public static void calculateDayWidth(Data data) {
		data.dayWidth = (data.WIDTH - data.headerWidth) / data.numberOfVisibleDays;
	}

	public static void calculateHourHeight(Data data) {
		data.hourHeight = (float)data.HEIGHT / (data.endHour - data.startHour);
	}

	private static void computeEventsRects(Data data, Context context) {
		List<WeekView.WeekViewEvent> allEvents = new ArrayList<>();
		AppPreferences prefs = new AppPreferences(context);
		for(Course c : data.courses) allEvents.add(Course.toWeekViewEvent(prefs, c));

		if(data.eventRects == null) data.eventRects = new ArrayList<>();
		else data.eventRects.clear();

		Collections.sort(allEvents, (event1, event2) -> {
			int comparator = event1.getStartTime().compareTo(event2.getStartTime());
			return comparator == 0 ? event1.getEndTime().compareTo(event2.getEndTime()) : comparator;
		});

		for(WeekView.WeekViewEvent event : allEvents)
			data.eventRects.add(new WeekView.EventRect(event));

		computePositionOfEvents(data);
	}

	private static void computePositionOfEvents(Data data) {
		// Make "collision groups" for all events that collide with others.
		List<List<WeekView.EventRect>> collisionGroups = new ArrayList<>();
		for(WeekView.EventRect eventRect : data.eventRects) {
			boolean isPlaced = false;

			outerLoop:
			for(List<WeekView.EventRect> collisionGroup : collisionGroups) {
				for(WeekView.EventRect groupEvent : collisionGroup) {
					if(groupEvent.event.isCollidingWith(eventRect.event)) {
						collisionGroup.add(eventRect);
						isPlaced = true;
						break outerLoop;
					}
				}
			}

			if(!isPlaced) {
				List<WeekView.EventRect> newGroup = new ArrayList<>();
				newGroup.add(eventRect);
				collisionGroups.add(newGroup);
			}
		}

		for(List<WeekView.EventRect> collisionGroup : collisionGroups)
			expandEventsToMaxWidth(collisionGroup);
	}

	private static void expandEventsToMaxWidth(List<WeekView.EventRect> collisionGroup) {
		// Expand the events to maximum possible width.
		List<List<WeekView.EventRect>> columns = new ArrayList<>();
		columns.add(new ArrayList<>());
		for(WeekView.EventRect eventRect : collisionGroup) {
			boolean isPlaced = false;
			for(List<WeekView.EventRect> column : columns) {
				if(column.size() == 0) {
					column.add(eventRect);
					isPlaced = true;
				} else if(!eventRect.event.isCollidingWith(column.get(column.size() - 1).event)) {
					column.add(eventRect);
					isPlaced = true;
					break;
				}
			}
			if(!isPlaced) {
				List<WeekView.EventRect> newColumn = new ArrayList<>();
				newColumn.add(eventRect);
				columns.add(newColumn);
			}
		}


		// Calculate left and right position for all the events.
		// Get the maxRowCount by looking in all columns.
		int maxRowCount = 0;
		for(List<WeekView.EventRect> column : columns)
			maxRowCount = Math.max(maxRowCount, column.size());
		for(int i = 0; i < maxRowCount; i++) {
			// Set the left and right values of the event.
			float j = 0;
			for(List<WeekView.EventRect> column : columns) {
				if(column.size() >= i + 1) {
					WeekView.EventRect eventRect = column.get(i);
					eventRect.width = 1f / columns.size();
					eventRect.left = j / columns.size();
				}
				j++;
			}
		}
	}

	private static Calendar getFirstDay(Data data, Calendar today) {
		Calendar day = (Calendar)today.clone();
		day.add(Calendar.DAY_OF_MONTH, data.dayOffset);
		while(day.get(Calendar.DAY_OF_WEEK) != data.firstDay) day.add(Calendar.DAY_OF_MONTH, -1);
		return day;
	}

	public static class Data {
		public int WIDTH;
		public int HEIGHT;

		public Paint eventPaint;
		public Paint headerPaint;
		public Paint headerTextPaint;
		public Paint backgroundPaint;
		public Paint horizontalLinePaint;
		public TextPaint eventTextPaint;

		public int startHour = 8;
		public int endHour = 17;
		public int dayOffset = 0;
		public int numberOfVisibleDays;
		public int firstDay = Calendar.MONDAY;
		public int lastDay = Calendar.FRIDAY;
		public int headerColor;
		public int headerTextColor;
		public int todayTextColor;
		public int defaultEventColor;
		public int eventTextColor;
		public int weekendBackgroundColor;
		public int futureBackgroundColor;
		public int pastBackgroundColor;
		public int todayBackgroundColor;
		public int horizontalLineColor;

		public float dayWidth;
		public float headerWidth;
		public float headerHeight;
		public float headerTextSize;
		public float eventTextSize;
		public float hourHeight = 100;
		public float eventCornerRadius = 5;
		public float overlappingEventsGap = 2;

		private List<Course> courses;
		private WeekView.Padding headerPadding;
		private WeekView.Padding eventTextPadding;
		private WeekView.DateTimeInterpreter dateTimeInterpreter;
		private List<WeekView.EventRect> eventRects;
	}
}
