package com.infinitemind.usoschedule.views;

import android.content.Context;
import android.widget.ImageView;

import com.infinitemind.usoschedule.R;
import com.skydoves.colorpickerpreference.ColorEnvelope;
import com.skydoves.colorpickerpreference.FlagView;

public class CustomFlagView extends FlagView {

	public CustomFlagView(Context context) {
		super(context, R.layout.view_custom_flag);
	}

	@Override
	public void onRefresh(ColorEnvelope colorEnvelope) {
		((ImageView) findViewById(R.id.colorPreview)).setColorFilter(colorEnvelope.getColor());
		findViewById(R.id.colorPreview).setRotation(getRotation());
	}
}