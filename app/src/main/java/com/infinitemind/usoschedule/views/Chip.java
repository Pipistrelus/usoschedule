package com.infinitemind.usoschedule.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.transition.AutoTransition;
import android.transition.TransitionManager;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.infinitemind.usoschedule.R;
import com.infinitemind.usoschedule.utils.Utils;

public class Chip extends FrameLayout {

	private boolean checkable;
	private boolean checked;
	private String text;
	private float textSize;
	private int checkedColor;
	private int uncheckedColor;
	private int checkedTextColor;
	private int uncheckedTextColor;
	private float checkedElevation;
	private float uncheckedElevation;

	private ViewGroup rootView;
	private CardView cardView;
	private ImageView dotImage;
	private ImageView closeImage;
	private AppCompatTextView textView;
	private OnCheckChangeListener onCheckedChangeListener;

	public Chip(@NonNull Context context, @Nullable AttributeSet attrs) {
		super(context, attrs);

		int dp1 = Utils.dpToPx(getContext(), 1);
		TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.CHIP, 0, 0);

		try {
			checkable = a.getBoolean(R.styleable.CHIP_chip_checkable, true);
			checked = a.getBoolean(R.styleable.CHIP_chip_checked, false);
			text = a.getString(R.styleable.CHIP_chip_text);
			textSize = a.getDimension(R.styleable.CHIP_chip_text_size, Utils.spToPx(getContext(), 14));
			checkedColor = a.getColor(R.styleable.CHIP_chip_checked_color, getResources().getColor(R.color.colorAccent));
			uncheckedColor = a.getColor(R.styleable.CHIP_chip_unchecked_color, getResources().getColor(R.color.colorWhite));
			checkedTextColor = a.getColor(R.styleable.CHIP_chip_checked_text_color, getResources().getColor(R.color.colorText2));
			uncheckedTextColor = a.getColor(R.styleable.CHIP_chip_unchecked_text_color, getResources().getColor(R.color.colorText1));
			checkedElevation = a.getDimension(R.styleable.CHIP_chip_checked_elevation, dp1 * 3);
			uncheckedElevation = a.getDimension(R.styleable.CHIP_chip_unchecked_elevation, dp1);
		} catch(Exception ignored) {}

		a.recycle();

		init();
	}

	private void init() {
		addView(rootView = (ViewGroup) inflate(getContext(), R.layout.view_chip, null));

		cardView = rootView.findViewById(R.id.cardView);
		textView = rootView.findViewById(R.id.textView);
		dotImage = rootView.findViewById(R.id.dotImage);
		closeImage = rootView.findViewById(R.id.closeImage);

		refreshState();

		cardView.setOnClickListener(view -> {
			setChecked(!checked);
			refreshState();
			if(onCheckedChangeListener != null)
				onCheckedChangeListener.onCheckChanged(checked);
		});
	}

	private void refreshState() {
		ConstraintLayout.LayoutParams layoutParams;
		if(!checkable || checked) {
			cardView.setCardElevation(checkedElevation);
			cardView.setCardBackgroundColor(checkedColor);
			textView.setTextColor(checkedTextColor);

			layoutParams = (ConstraintLayout.LayoutParams) dotImage.getLayoutParams();
			layoutParams.leftToRight = textView.getId();
			layoutParams.leftToLeft = ConstraintLayout.LayoutParams.UNSET;
			dotImage.setLayoutParams(layoutParams);

			layoutParams = (ConstraintLayout.LayoutParams) textView.getLayoutParams();
			layoutParams.leftToLeft = ConstraintLayout.LayoutParams.PARENT_ID;
			layoutParams.leftToRight = ConstraintLayout.LayoutParams.UNSET;
			textView.setLayoutParams(layoutParams);

			closeImage.setScaleX(1);
			closeImage.setScaleY(1);
			dotImage.setScaleX(1);
			dotImage.setScaleY(1);
			dotImage.setColorFilter(uncheckedColor);
		} else {
			cardView.setCardElevation(uncheckedElevation);
			cardView.setCardBackgroundColor(uncheckedColor);
			textView.setTextColor(uncheckedTextColor);

			layoutParams = (ConstraintLayout.LayoutParams) dotImage.getLayoutParams();
			layoutParams.leftToRight = ConstraintLayout.LayoutParams.UNSET;
			layoutParams.leftToLeft = ConstraintLayout.LayoutParams.PARENT_ID;
			dotImage.setLayoutParams(layoutParams);

			layoutParams = (ConstraintLayout.LayoutParams) textView.getLayoutParams();
			layoutParams.leftToLeft = ConstraintLayout.LayoutParams.UNSET;
			layoutParams.leftToRight = dotImage.getId();
			textView.setLayoutParams(layoutParams);

			closeImage.setScaleX(0);
			closeImage.setScaleY(0);
			dotImage.setScaleX(0.5f);
			dotImage.setScaleY(0.5f);
			dotImage.setColorFilter(checkedColor);
		}

		if(!checkable) {
			closeImage.setVisibility(GONE);
			dotImage.setVisibility(GONE);
		}

		closeImage.setColorFilter(checkedColor);
		textView.setText(text);
		textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);

		cardView.post(() -> cardView.setRadius(getHeight() / 3f));

		TransitionManager.beginDelayedTransition(rootView, new AutoTransition());
	}

	////////////////////  SETTERS  ////////////////////
	public void setText(String text) {
		this.text = text;
	}

	public void setCheckable(boolean checkable) {
		this.checkable = checkable;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public void setCheckedColor(int checkedColor) {
		this.checkedColor = checkedColor;
		refreshState();
	}

	public void setUncheckedColor(int uncheckedColor) {
		this.uncheckedColor = uncheckedColor;
		refreshState();
	}

	public void setCheckedTextColor(int checkedTextColor) {
		this.checkedTextColor = checkedTextColor;
		refreshState();
	}

	public void setUncheckedTextColor(int uncheckedTextColor) {
		this.uncheckedTextColor = uncheckedTextColor;
		refreshState();
	}

	public void setCheckedElevation(float checkedElevation) {
		this.checkedElevation = checkedElevation;
		refreshState();
	}

	public void setUncheckedElevation(float uncheckedElevation) {
		this.uncheckedElevation = uncheckedElevation;
		refreshState();
	}

	public void setOnCheckedChangeListener(OnCheckChangeListener onCheckedChangeListener) {
		this.onCheckedChangeListener = onCheckedChangeListener;
	}

	////////////////////  GETTERS  ////////////////////
	public String getText() {
		return text;
	}

	public boolean isCheckable() {
		return checkable;
	}

	public boolean isChecked() {
		return checked;
	}

	public int getCheckedColor() {
		return checkedColor;
	}

	public int getUncheckedColor() {
		return uncheckedColor;
	}

	public int getCheckedTextColor() {
		return checkedTextColor;
	}

	public int getUncheckedTextColor() {
		return uncheckedTextColor;
	}

	public float getCheckedElevation() {
		return checkedElevation;
	}

	public float getUncheckedElevation() {
		return uncheckedElevation;
	}

	public interface OnCheckChangeListener {
		void onCheckChanged(boolean checked);
	}
}
