package com.infinitemind.usoschedule.utils;

import android.view.View;

public class Transformer implements GalleryLayoutManager.ItemTransformer {

	private final float minScale;
	private final float maxMinDiff;
	private final float minAlpha;

	public Transformer() {
		minScale = 0.8f;
		maxMinDiff = 0.2f;
		minAlpha = 0.5f;
	}

	@Override
	public void transformItem(GalleryLayoutManager layoutManager, View item, float fraction) {
		float closenessToCenter = 1f - Math.abs(fraction);
		float scale = minScale + maxMinDiff * closenessToCenter;
		item.setScaleX(scale);
		item.setScaleY(scale);
		float alpha = Utils.map(scale, maxMinDiff + minScale, minScale, 1, minAlpha);
		item.setAlpha(alpha);
	}
}
