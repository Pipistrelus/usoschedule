package com.infinitemind.usoschedule.utils;

import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class BottomPaddingItemDecoration extends RecyclerView.ItemDecoration {
	private final int bottomPadding;

	public BottomPaddingItemDecoration(int bottomPadding) {
		this.bottomPadding = bottomPadding;
	}

	@Override
	public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
		if(parent.getAdapter() != null && parent.getChildAdapterPosition(view) == parent.getAdapter().getItemCount() - 1)
			outRect.set(0, 0, 0, bottomPadding);
	}
}