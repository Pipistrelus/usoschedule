package com.infinitemind.usoschedule.utils;

import com.infinitemind.usoschedule.api.UsosApi;
import com.infinitemind.usoschedule.model.University;

import org.scribe.builder.ServiceBuilder;
import org.scribe.model.SignatureType;
import org.scribe.oauth.OAuthService;

public class OAuthHelper {

    public static OAuthService getService(University university) {
        return new ServiceBuilder()
                .provider(UsosApi.withScopes(university, "studies", "email", "other_emails", "grades", "offline_access"))
                .apiKey(university.consumerKey)
                .apiSecret(university.consumerSecret)
                .signatureType(SignatureType.QueryString)
                .callback("usoschedule://callback")
                .build();
    }
}
