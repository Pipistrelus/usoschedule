package com.infinitemind.usoschedule.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.LabeledIntent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.annotation.ColorInt;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.AppCompatTextView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.infinitemind.usoschedule.R;
import com.infinitemind.usoschedule.activities.CopyToClipboard;
import com.infinitemind.usoschedule.model.Building;
import com.infinitemind.usoschedule.model.CourseGrade;
import com.infinitemind.usoschedule.model.Filter;
import com.infinitemind.usoschedule.model.Group;
import com.infinitemind.usoschedule.model.LatLong;
import com.infinitemind.usoschedule.model.StudentProgramme;
import com.infinitemind.usoschedule.model.University;
import com.infinitemind.usoschedule.model.User;
import com.infinitemind.usoschedule.runnable.DataCallback;
import com.infinitemind.usoschedule.runnable.Timer;
import com.tapadoo.alerter.Alerter;
import com.tooltip.Tooltip;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Comparator;
import java.util.Locale;

public class Utils {
	public static final Comparator<StudentProgramme> studentProgrammeComparator = (p1, p2) -> p1.programme.description.getName().compareTo(p2.programme.description.getName());
	public static final Comparator<Group> groupComparator = (g1, g2) -> g1.course_name.getName().compareTo(g2.course_name.getName());
	public static final Comparator<User> userComparator = (u1, u2) -> u1.getUserName(true).compareTo(u2.getUserName(true));
	public static final Comparator<Filter> filterComparator = (f1, f2) -> f1.getValue().compareTo(f2.getValue());
	public static final Comparator<Building> buildingsComparator = (b1, b2) -> b1.name.getName().compareTo(b2.name.getName());
	public static final Comparator<University> universitiesComparator = University::compareTo;
	public static final Comparator<CourseGrade> gradesComparator = (g1, g2) -> g1.exam_session_number - g2.exam_session_number;

	public static final int UNDEFINED_WINDOW_SIZE = Integer.MAX_VALUE;

	public static void makeDialog(Activity activity, @LayoutRes int layout, DataCallback<Dialog> callback, int... windowSize) {
		Dialog dialog = new Dialog(activity);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		Window window = dialog.getWindow();
		if(window != null) {
			window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
			window.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
			WindowManager.LayoutParams layoutParams = dialog.getWindow().getAttributes();
			layoutParams.dimAmount = 0.5f;
			dialog.getWindow().setAttributes(layoutParams);
		}
		Point size = getScreenSize(activity);
		int width = windowSize != null && windowSize.length > 0 && windowSize[0] != UNDEFINED_WINDOW_SIZE ? windowSize[0] : Math.min(dpToPx(activity, 350), size.x - 150);
		int height = windowSize != null && windowSize.length > 1 && windowSize[1] != UNDEFINED_WINDOW_SIZE ? windowSize[1] : Math.min(dpToPx(activity, 500), size.y - 150);
		ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(width, height);
		LayoutInflater inflater = LayoutInflater.from(activity);
		View view = inflater.inflate(layout, null);
		dialog.setContentView(view, layoutParams);
		callback.run(dialog);
		if(!dialog.isShowing())
			dialog.show();
	}

	public static Point getScreenSize(Activity activity) {
		DisplayMetrics displayMetrics = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
		return new Point(displayMetrics.widthPixels, displayMetrics.heightPixels);
	}

	public static void showTooltip(View view) {
		Context context = view.getContext();
		Resources resources = context.getResources();
		Tooltip tooltip = new Tooltip.Builder(view)
				.setText(resources.getString(resources.getIdentifier(resources.getResourceEntryName(view.getId()), "string", context.getPackageName())))
				.setTypeface(ResourcesCompat.getFont(context, R.font.arcon))
				.setArrowHeight(10f)
				.setBackgroundColor(resources.getColor(R.color.colorText1))
				.setTextColor(resources.getColor(R.color.colorText2))
				.setPadding(20f)
				.setCornerRadius(10f)
				.setTextSize(18f)
				.show();

		new Timer(1000, tooltip::dismiss).start();
	}

	public static int dpToPx(Context context, int dp) {
		if(context != null)
			return (int) (context.getResources().getDisplayMetrics().density * dp);
		else return 0;
	}

	public static int spToPx(Context context, int sp) {
		if(context != null)
			return (int) (context.getResources().getDisplayMetrics().scaledDensity * sp);
		else return 0;
	}

	public static float map(float value, float istart, float istop, float ostart, float ostop) {
		return ostart + (ostop - ostart) * ((value - istart) / (istop - istart));
	}

	public static boolean isOffline(Context context) {
		ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		if(connectivity != null) {
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if(info != null) for(NetworkInfo anInfo : info)
				if(anInfo.getState() == NetworkInfo.State.CONNECTED) return false;
		}
		return true;
	}

	public static void emailAt(Context context, String email) {
		Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", email, null));

		User user = new AppPreferences(context).getUserInfo();
		if(user != null) {
			emailIntent.putExtra(Intent.EXTRA_TEXT, context.getResources().getString(R.string.regards, user.getUserName(true), user.student_number));
		}

		LabeledIntent labeledCopyToClipboard = new LabeledIntent(new Intent(context, CopyToClipboard.class).putExtra(Intent.EXTRA_TEXT, email), context.getPackageName(), context.getResources().getString(R.string.copy_to_clipboard), R.drawable.ic_copy);

		Intent chooserIntent = Intent.createChooser(emailIntent, context.getResources().getString(R.string.send_email));
		chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] { labeledCopyToClipboard });
		context.startActivity(chooserIntent);
	}

	public static boolean isPackageExisting(Context context, String targetPackage){
		try {
			context.getPackageManager().getPackageInfo(targetPackage,PackageManager.GET_META_DATA);
		} catch (PackageManager.NameNotFoundException e) {
			return false;
		}
		return true;
	}

	public static String getAssetFile(Context context, String fileName) {
		StringBuilder output = new StringBuilder();
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(context.getAssets().open(fileName), StandardCharsets.UTF_8));

			String mLine;
			while ((mLine = reader.readLine()) != null) output.append(mLine);
		} catch (IOException ignored) {
		} finally {
			if (reader != null) try {
				reader.close();
			} catch(IOException ignored) { }
		}
		return output.toString();
	}

	public static void showBuildingOnMap(Activity activity, @Nullable Building building, int color) {
		if(building == null || building.location == null || building.name == null) {
			Typeface arconFont = ResourcesCompat.getFont(activity.getApplicationContext(), R.font.arcon);
			if(arconFont != null)
				Alerter.create(activity)
						.setBackgroundColorInt(color)
						.setTitle(activity.getResources().getString(R.string.message))
						.setText(activity.getResources().getString(R.string.address_failure))
						.enableSwipeToDismiss()
						.setTitleTypeface(arconFont)
						.setTextTypeface(arconFont)
						.setDuration(3000)
						.show();
		} else {
			String googleMapsPackageName = "com.google.android.apps.maps";
			if(!isPackageExisting(activity, googleMapsPackageName)) {
				makeDialog(activity, R.layout.dialog_show_message, dialog -> {
					if(dialog == null) return;

					((AppCompatTextView) dialog.findViewById(R.id.title)).setText(activity.getResources().getString(R.string.message));
					((AppCompatTextView) dialog.findViewById(R.id.message)).setText(activity.getResources().getString(R.string.download_maps));

				}, Utils.UNDEFINED_WINDOW_SIZE, ViewGroup.LayoutParams.WRAP_CONTENT);
			} else {
				LatLong latlng = building.location;
				String format = String.format(Locale.getDefault(), "%g,%g", latlng.lat, latlng.lng);
				Uri query = Uri.parse("geo:0,0?q=" + format + "(" + Uri.encode(building.name.getName()) + ")");
				activity.startActivity(new Intent(Intent.ACTION_VIEW, query).setPackage(googleMapsPackageName));
			}
		}
	}

	public static void showBuildingOnMap(Activity activity, @Nullable Building building) {
		showBuildingOnMap(activity, building, activity.getResources().getColor(R.color.color1));
	}

	public static int getDarkerColor(@ColorInt int color, float scale) {
		float[] hsv = new float[3];
		Color.colorToHSV(color, hsv);
		hsv[2] *= scale;
		return Color.HSVToColor(hsv);
	}
}