package com.infinitemind.usoschedule.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.annotation.ColorInt;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.infinitemind.usoschedule.R;
import com.infinitemind.usoschedule.model.Course;
import com.infinitemind.usoschedule.model.CourseGrade;
import com.infinitemind.usoschedule.model.Group;
import com.infinitemind.usoschedule.model.User;

import org.scribe.extractors.TokenExtractorImpl;
import org.scribe.model.Token;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.annotation.Nullable;

public class AppPreferences {

	public static final SimpleDateFormat dateFormat = new SimpleDateFormat("d.MM.yyyy");

	private static final String DEFAULT_VISIBLE_DAYS = "default_visible_days";
	private static final String EVENT_TEXT_SIZE = "event_text_size";
	private static final String WIDGET_FIRST_DAY = "widget_first_day";
	private static final String WIDGET_LAST_DAY = "widget_last_day";
	private static final String BREAK_LENGTH = "break_length";
	private static final String GROUPS = "course_editions";
	private static final String GRADES = "grades";
	private static final String TIMETABLE = "timetables";
	private static final String RECENTS = "recents";
	private static final String COLORS = "colors";
	private static final String NIGHT_MODE = "night_mode";

	private static final String PREFERENCE_NAME = "app_prefs";
	private static final String ACCESS_TOKEN = "access_token";

	private static final String UNIVERSITY_ID = "university_id";

	private static final String USER_INFO = "user_info";
	private static Gson gson;

	private final SharedPreferences mPrefs;
	private final Context context;

	public static Gson getGson() {
		return gson == null ? gson = new Gson() : gson;
	}

	public AppPreferences(Context context) {
		this.context = context;
		mPrefs = context.getSharedPreferences(PREFERENCE_NAME, 0);
	}

	public Editor edit() {
		return new Editor(mPrefs);
	}

	@Nullable public Token getAccessToken() {
		String rawResponse = mPrefs.getString(ACCESS_TOKEN, null);
		if(rawResponse != null) return new TokenExtractorImpl().extract(rawResponse);
		return null;
	}

	public String getTokenResponse(String token) {
		return mPrefs.getString(token, null);
	}

	@Nullable public User getUserInfo() {
		String user = mPrefs.getString(USER_INFO, "");
		return user != null && !user.isEmpty() ? getGson().fromJson(user, User.class) : null;
	}

	public User getUserInfoById(String id) {
		String user = mPrefs.getString(USER_INFO.concat("_" + id), "");
		return user != null && !user.isEmpty() ? getGson().fromJson(user, User.class) : null;
	}

	public List<Course> getTimetableForDays(Calendar temp, int days, int bias) {
		Calendar date = (Calendar)temp.clone();
		date.add(Calendar.DAY_OF_MONTH, bias);

		ArrayList<Course> timetable = new ArrayList<>();
		for(int i = 0; i < days; i++, date.add(Calendar.DAY_OF_MONTH, 1)) {
			List<Course> courses = getTimetableForDay(date);
			if(courses != null) timetable.addAll(courses);
		}
		return timetable;
	}

	@Nullable private List<Course> getTimetableForDay(Calendar day) {
		String array = mPrefs.getString(TIMETABLE.concat("_" + dateFormat.format(day.getTime())), "[]");
		return getGson().fromJson(array, new TypeToken<List<Course>>() {}.getType());
	}

	@Nullable public List<Group> getGroups() {
		String groups = mPrefs.getString(GROUPS, "");
		return groups != null && !groups.isEmpty() ? getGson().fromJson(groups, new TypeToken<List<Group>>(){}.getType()) : null;
	}

	@Nullable public List<CourseGrade> getGrades(String termId, List<String> courseIds) {
		ArrayList<CourseGrade> grades = new ArrayList<>();
		for(String courseId : courseIds) {
			String key = GRADES.concat("_" + termId).concat("_" + courseId);
			for(int i = 0; i < mPrefs.getInt(key, 0); i++)
				grades.add(getGson().fromJson(mPrefs.getString(key.concat("_" + i), ""), CourseGrade.class));
		}
		return grades.isEmpty() ? null : grades;
	}

	public ArrayList<User> getRecentlySearched() {
		ArrayList<User> users = new ArrayList<>();
		for(int i = 0; i < mPrefs.getInt(RECENTS, 0); i++)
			users.add(getGson().fromJson(mPrefs.getString(RECENTS.concat("_" + i), ""), User.class));
		return users;
	}

	@ColorInt public int getEventColorForClassType(String classType) {
		int color = mPrefs.getInt(COLORS.concat("_" + classType), -1);
		if(color == -1) {
			switch(classType.toLowerCase()) {
				case "wykład": return Color.parseColor("#FF80CBC4");
				case "ćwiczenia": return Color.parseColor("#FFF06292");
				case "laboratorium": return Color.parseColor("#FFFF9E80");
				case "wf": return Color.parseColor("#FF81C784");
			}
			return Color.parseColor("#FFAED581");
		}
		return color;
	}

	public int getBreakLength() {
		return mPrefs.getInt(BREAK_LENGTH, context.getResources().getInteger(R.integer.default_break_length));
	}

	public int getEventTextSize() {
		return mPrefs.getInt(EVENT_TEXT_SIZE, context.getResources().getInteger(R.integer.default_event_text_size));
	}

	public int getDefaultVisibleDays() {
		return mPrefs.getInt(DEFAULT_VISIBLE_DAYS, context.getResources().getInteger(R.integer.default_visible_days));
	}

	public int getWidgetFirstDay() {
		return mPrefs.getInt(WIDGET_FIRST_DAY, context.getResources().getInteger(R.integer.widget_first_day));
	}

	public int getWidgetLastDay() {
		return mPrefs.getInt(WIDGET_LAST_DAY, context.getResources().getInteger(R.integer.widget_last_day));
	}

	public boolean isNightModeEnabled() {
		return mPrefs.getBoolean(NIGHT_MODE, false);
	}

	public boolean hasAccessToken() {
		return getAccessToken() != null;
	}

	public int getUniversityId() {
		return mPrefs.getInt(UNIVERSITY_ID, 0);
	}

	public boolean isTimetableForDay(Calendar day) {
		return mPrefs.contains(TIMETABLE.concat("_" + dateFormat.format(day.getTime())));
	}

	public static class Editor {
		private final SharedPreferences.Editor mEditor;
		private final SharedPreferences mPrefs;

		@SuppressLint("CommitPrefEdits")
		Editor(SharedPreferences mPrefs) {
			this.mPrefs = mPrefs;
			mEditor = mPrefs.edit();
		}

		public void setTokenResponse(Token token) {
			mEditor.putString(token.getToken(), token.getRawResponse());
			mEditor.apply();
		}

		public void setAccessToken(Token token) {
			String rawResponse = token == null ? null : token.getRawResponse();
			mEditor.putString(ACCESS_TOKEN, rawResponse);
			mEditor.apply();
		}

		public void setUserInfo(User user) {
			mEditor.putString(USER_INFO, getGson().toJson(user));
			mEditor.apply();
		}

		public void setUserInfoById(User user, String id) {
			mEditor.putString(USER_INFO.concat("_" + id), getGson().toJson(user));
			mEditor.apply();
		}

		public void setGroups(List<Group> groups) {
			mEditor.putString(GROUPS, getGson().toJson(groups));
			mEditor.apply();
		}

		public void setGrades(String termId, List<CourseGrade> grades) {
			for(CourseGrade grade : grades) {
				String key = GRADES.concat("_" + termId).concat("_" + grade.course_id);
				int numberOfGrades = mPrefs.getInt(key, 0);
				mEditor.putInt(key, numberOfGrades + 1);
				mEditor.putString(key.concat("_" + numberOfGrades), getGson().toJson(grade));
			}

			mEditor.commit();
		}

		public void setTimetableForDays(Calendar date, List<Course> timetable, int days) {
			ArrayList<Course> temp = new ArrayList<>(timetable);
			for(int i = 0; i < days; i++, date.add(Calendar.DAY_OF_MONTH, 1)) {
				String format = "_" + dateFormat.format(date.getTime());
				List<Course> array = new ArrayList<>();
				for(int j = temp.size() - 1; j >= 0; j--)
					if(format.equals("_" + dateFormat.format(temp.get(j).start_time)))
						array.add(temp.remove(j));

				mEditor.putString(TIMETABLE.concat(format), getGson().toJson(array));
			}

			mEditor.apply();
		}

		public void addRecentlySearched(User user) {
			int index = mPrefs.getInt(RECENTS, 0);

			mEditor.putInt(RECENTS, index + 1);
			mEditor.putString(RECENTS.concat("_" + index), getGson().toJson(user));

			mEditor.apply();
		}

		public void setRecentlySearched(List<User> recentlySearched) {
			mEditor.putInt(RECENTS, recentlySearched.size());
			for(int i = 0; i < recentlySearched.size(); i++)
				mEditor.putString(RECENTS.concat("_" + i), getGson().toJson(recentlySearched.get(i)));
			mEditor.apply();
		}

		public void setEventColorForClassType(String classType, @ColorInt int color) {
			mEditor.putInt(COLORS.concat("_" + classType), color);

			mEditor.commit();
		}

		public void setBreakLength(int length) {
			mEditor.putInt(BREAK_LENGTH, length);

			mEditor.commit();
		}

		public void setEventTextSize(int textSize) {
			mEditor.putInt(EVENT_TEXT_SIZE, textSize);

			mEditor.commit();
		}

		public void setDefaultVisibleDays(int defaultVisibleDays) {
			mEditor.putInt(DEFAULT_VISIBLE_DAYS, defaultVisibleDays);

			mEditor.commit();
		}

		public void setWidgetFirstDay(int firstDay) {
			mEditor.putInt(WIDGET_FIRST_DAY, firstDay);

			mEditor.commit();
		}

		public void setWidgetLastDay(int lastDay) {
			mEditor.putInt(WIDGET_LAST_DAY, lastDay);

			mEditor.commit();
		}

		public void setNightModeEnabled(boolean enabled) {
			mEditor.putBoolean(NIGHT_MODE, enabled);

			mEditor.commit();
		}

		public Editor clear() {
			mEditor.clear();
			return this;
		}

		public Editor setUniversityId(int id) {
			mEditor.putInt(UNIVERSITY_ID, id);
			return this;
		}

		public void commit() {
			mEditor.commit();
		}
	}
}
