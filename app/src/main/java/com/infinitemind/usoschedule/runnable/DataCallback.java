package com.infinitemind.usoschedule.runnable;

import android.support.annotation.Nullable;

public interface DataCallback<T> {
	void run(@Nullable T data);
}
