package com.infinitemind.usoschedule.runnable;

import android.text.Editable;
import android.text.TextWatcher;

public class OnTextChanged implements TextWatcher {

	private final DataCallback<Editable> runnable;

	public OnTextChanged(DataCallback<Editable> runnable) {
		this.runnable = runnable;
	}

	@Override
	public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

	}

	@Override
	public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

	}

	@Override
	public void afterTextChanged(Editable editable) {
		runnable.run(editable);
	}
}
