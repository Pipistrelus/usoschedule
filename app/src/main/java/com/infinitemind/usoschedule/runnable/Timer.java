package com.infinitemind.usoschedule.runnable;

import android.os.CountDownTimer;

public class Timer extends CountDownTimer {

	private final Runnable callback;

	public Timer(long millisInFuture, Runnable callback) {
		super(millisInFuture, millisInFuture);
		this.callback = callback;
	}

	@Override
	public void onTick(long l) {

	}

	@Override
	public void onFinish() {
		callback.run();
	}
}
