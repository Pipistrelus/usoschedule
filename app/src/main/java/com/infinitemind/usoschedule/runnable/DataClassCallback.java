package com.infinitemind.usoschedule.runnable;

import android.support.annotation.Nullable;

public abstract class DataClassCallback<T> implements DataCallback<T> {
	private Object object;

	@Override abstract public void run(@Nullable T data);

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}
}
