package com.infinitemind.usoschedule.runnable;

import android.widget.SeekBar;

public class OnSeekBarChanged implements SeekBar.OnSeekBarChangeListener {

	private final DataCallback<Integer> onProgressChangedListener;

	public OnSeekBarChanged(DataCallback<Integer> onProgressChangedListener) {
		this.onProgressChangedListener = onProgressChangedListener;
	}

	@Override public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
		onProgressChangedListener.run(progress);
	}

	@Override public void onStartTrackingTouch(SeekBar seekBar) {

	}

	@Override public void onStopTrackingTouch(SeekBar seekBar) {

	}
}
