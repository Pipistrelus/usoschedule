package com.infinitemind.usoschedule.runnable;

import android.support.design.widget.TabLayout;

public class OnTabSelected implements TabLayout.OnTabSelectedListener {

	private DataCallback<TabLayout.Tab> callback;

	public OnTabSelected(DataCallback<TabLayout.Tab> callback) {
		this.callback = callback;
	}

	@Override public void onTabSelected(TabLayout.Tab tab) {
		callback.run(tab);
	}

	@Override public void onTabUnselected(TabLayout.Tab tab) {

	}

	@Override public void onTabReselected(TabLayout.Tab tab) {

	}
}
