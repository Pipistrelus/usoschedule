package com.infinitemind.usoschedule.settings;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import com.infinitemind.usoschedule.R;
import com.infinitemind.usoschedule.runnable.DataCallback;

public class SettingsItem<T> extends FrameLayout {

	DataCallback<T> onClickListener;
	protected AppCompatTextView textSubtitle;
	protected AppCompatTextView textTitle;
	protected AppCompatImageView image;
	protected View divider;
	private String title;
	private String subtitle;

	public SettingsItem(Context context) {
		super(context);
		initializeLayout(R.layout.item_settings);

		textTitle = findViewById(R.id.textTitle);
		textSubtitle = findViewById(R.id.textSubtitle);
		image = findViewById(R.id.image);
		divider = findViewById(R.id.divider);

		if(hasRipple()) {
			setClickable(true);
			TypedArray ta = context.obtainStyledAttributes(new int[]{android.R.attr.selectableItemBackground});
			setForeground(ta.getDrawable(0));
			ta.recycle();
		}

		if(getType() == Type.withImage || getType() == Type.simple) image.setVisibility(VISIBLE);
	}

	public void initializeLayout(int layout) {
		removeAllViewsInLayout();
		View inflated = LayoutInflater.from(getContext()).inflate(layout, this);
		inflated.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.WRAP_CONTENT));
		inflated.measure(MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED), MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
		inflated.layout(0, 0, inflated.getMeasuredWidth(), inflated.getMeasuredHeight());
	}

	public void refreshTitle() {
		if(textTitle == null) return;
		textTitle.setText(getTitle());
	}

	public void refreshSubtitle() {
		if(textSubtitle == null) return;
		textSubtitle.setText(getSubtitle());
	}

	public SettingsItem refresh() {
		refreshTitle();
		refreshSubtitle();
		return this;
	}

	public SettingsItem setTitle(String title) {
		this.title = title;
		return this;
	}

	public String getTitle() {
		return title;
	}

	public SettingsItem setSubtitle(String subtitle) {
		this.subtitle = subtitle;
		return this;
	}

	public String getSubtitle() {
		return subtitle;
	}

	@Nullable public AppCompatImageView getImage() {
		return image;
	}

	@Nullable public AppCompatTextView getTextTitle() {
		return textTitle;
	}

	@Nullable public AppCompatTextView getTextSubtitle() {
		return textSubtitle;
	}

	public SettingsItem<T> hideDivider() {
		if(divider == null) return this;
		divider.setVisibility(GONE);
		return this;
	}

	public SettingsItem<T> setOnClickListener(DataCallback<T> onClickListener) {
		this.onClickListener = onClickListener;
		return this;
	}

	public SettingsItem<T> toggle(boolean enabled) {
		super.setEnabled(enabled);
		setAlpha(enabled ? 1 : 0.3f);
		setClickable(enabled);
		return this;
	}

	public Type getType() {
		return Type.simple;
	}

	protected boolean hasRipple() {
		return true;
	}

	public enum Type {
		simple, withImage
	}
}
