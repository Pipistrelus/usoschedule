package com.infinitemind.usoschedule.settings;

import android.content.Context;

import com.infinitemind.usoschedule.R;

public class HeaderItem extends SettingsItem<Void> {

	public HeaderItem(Context context) {
		super(context);

		initializeLayout(R.layout.item_header);

		textTitle = findViewById(R.id.textTitle);
	}

	@Override protected boolean hasRipple() {
		return false;
	}
}
