package com.infinitemind.usoschedule.settings;

import android.content.Context;
import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;
import android.support.v7.widget.AppCompatImageView;

import com.infinitemind.usoschedule.R;

public class ImageItem extends SettingsItem<Void> {

	private AppCompatImageView image;

	public ImageItem(Context context) {
		super(context);

		image = getImage();

		setOnClickListener((OnClickListener) view -> onClickListener.run(null));
	}

	public ImageItem setImageColor(@ColorInt int color) {
		if(image == null) return this;
		image.setColorFilter(color);
		return this;
	}

	public ImageItem setImageResource(@DrawableRes int id) {
		if(image == null) return this;
		image.setImageResource(id);
		image.setColorFilter(getResources().getColor(R.color.colorTransparent));
		return this;
	}

	@Override
	public Type getType() {
		return Type.withImage;
	}
}
