package com.infinitemind.usoschedule.api;

import android.content.Context;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.infinitemind.usoschedule.utils.AppPreferences;
import com.infinitemind.usoschedule.utils.OAuthHelper;
import com.infinitemind.usoschedule.model.Universities;

import org.apache.commons.httpclient.HttpStatus;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

public abstract class AbsUsosApi {

    private final OAuthService mService;
    private final Token mToken;
    private final AppPreferences mAppPreferences;
    private Gson mGson;

    private DateFormat mDateFormat;

    AbsUsosApi(Context context) {
        mAppPreferences = new AppPreferences(context);
        int id = mAppPreferences.getUniversityId();
        mService = OAuthHelper.getService(Universities.getUniversity(id));
        mToken = mAppPreferences.getAccessToken();
        initGson();
    }

    private void initGson() {
        mGson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                .create();
    }

    public Response execute(OAuthRequest request) {
        mService.signRequest(mToken, request);
        Response response = request.send();

        if (response == null || response.getCode() == HttpStatus.SC_UNAUTHORIZED) {
            mAppPreferences.edit().setAccessToken(null);
            return null;
        }

        return response;
    }

    public Gson getGson() {
        return mGson;
    }

    DateFormat getDateFormat() {
        if (mDateFormat == null) {
            mDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        }

        return mDateFormat;
    }
}
