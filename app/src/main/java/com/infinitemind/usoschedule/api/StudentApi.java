package com.infinitemind.usoschedule.api;

import android.content.Context;
import android.net.Uri;

import com.google.common.base.Joiner;
import com.infinitemind.usoschedule.utils.AppPreferences;
import com.infinitemind.usoschedule.model.Universities;
import com.infinitemind.usoschedule.model.University;

import org.scribe.model.OAuthRequest;
import org.scribe.model.Verb;

import java.util.Calendar;
import java.util.Date;

public class StudentApi extends AbsUsosApi {

	private final University mUniversity;

	public StudentApi(Context context) {
		super(context);
		int id = new AppPreferences(context).getUniversityId();
		mUniversity = Universities.getUniversity(id);
	}

	public OAuthRequest getTimeTableRequest(Calendar startDate, int days) {
		Uri uri = Uri.parse(mUniversity.getServiceUrl() + "/tt/user").buildUpon()
				.appendQueryParameter("start", getDateFormat().format(startDate.getTime()))
				.appendQueryParameter("days", String.valueOf(days))
				.appendQueryParameter("fields", "start_time|end_time|name|course_id|building_name|building_id|room_number|group_number|room_id|frequency|classtype_id|unit_id|classtype_name|lecturer_ids")
				.build();

		return new OAuthRequest(Verb.GET, uri.toString());
	}

	public OAuthRequest getTimeTableRequest(String id, Calendar startDate, int days) {
		Uri uri = Uri.parse(mUniversity.getServiceUrl() + "/tt/staff").buildUpon()
				.appendQueryParameter("user_id", id)
				.appendQueryParameter("start", getDateFormat().format(startDate.getTime()))
				.appendQueryParameter("days", String.valueOf(days))
				.appendQueryParameter("fields", "start_time|end_time|name|course_id|building_name|building_id|room_number|group_number|room_id|frequency|classtype_id|unit_id|classtype_name|lecturer_ids")
				.build();

		return new OAuthRequest(Verb.GET, uri.toString());
	}

	public OAuthRequest getTimetableForCourseRequest(String termId, String courseId, Calendar startDate, int days) {
		Uri uri = Uri.parse(mUniversity.getServiceUrl() + "/tt/course_edition").buildUpon()
				.appendQueryParameter("fields", "start_time|end_time|name")
				.appendQueryParameter("start", getDateFormat().format(startDate.getTime()))
				.appendQueryParameter("days", String.valueOf(days))
				.appendQueryParameter("course_id", courseId)
				.appendQueryParameter("term_id", termId)
				.build();

		return new OAuthRequest(Verb.GET, uri.toString());
	}

	public OAuthRequest getUserGroupsRequest() {
		Uri uri = Uri.parse(mUniversity.getServiceUrl() + "/groups/user").buildUpon()
				.appendQueryParameter("fields", "term_id|course_id|class_type|course_name|group_number|participants|lecturers")
				.appendQueryParameter("active_terms", "true")
				.build();

		return new OAuthRequest(Verb.GET, uri.toString());
	}


	public OAuthRequest getGradesRequest(String termId, String courseId) {
		Uri uri = Uri.parse(mUniversity.getServiceUrl() + "/grades/course_edition").buildUpon()
				.appendQueryParameter("fields", "value_symbol|exam_id|exam_session_number|date_modified|counts_into_average|grade_type_id")
				.appendQueryParameter("course_id", courseId)
				.appendQueryParameter("term_id", termId)
				.build();

		return new OAuthRequest(Verb.GET, uri.toString());
	}

	public OAuthRequest getFindingRequest(String query, int start) {
		Uri uri = Uri.parse(mUniversity.getServiceUrl() + "/users/search2").buildUpon()
				.appendQueryParameter("lang", "pl|en")
				.appendQueryParameter("among", "all")
				.appendQueryParameter("num", "20")
				.appendQueryParameter("start", String.valueOf(start))
				.appendQueryParameter("fields", "items[match|user[id|titles|first_name|last_name|email]]|next_page")
				.appendQueryParameter("query", query)
				.build();
		return new OAuthRequest(Verb.GET, uri.toString());
	}

	public OAuthRequest getBuildingsRequest() {
		Uri uri = Uri.parse(mUniversity.getServiceUrl() + "/geo/building_index").buildUpon()
				.appendQueryParameter("fields", "id|name|postal_address|location|campus_name")
				.build();

		return new OAuthRequest(Verb.GET, uri.toString());
	}

	public OAuthRequest getBuildingRequest(String id) {
		Uri uri = Uri.parse(mUniversity.getServiceUrl() + "/geo/building2").buildUpon()
				.appendQueryParameter("building_id", id)
				.appendQueryParameter("fields", "id|name|postal_address|location|campus_name")
				.build();

		return new OAuthRequest(Verb.GET, uri.toString());
	}

	public OAuthRequest getUserDetailRequest(String id) {
		Uri uri = Uri.parse(mUniversity.getServiceUrl() + "/users/user").buildUpon()
				.appendQueryParameter("user_id", id)
				.appendQueryParameter("fields", "id|titles|first_name|last_name|office_hours|interests|email")
				.build();

		return new OAuthRequest(Verb.GET, uri.toString());
	}

	public OAuthRequest getUserDetailRequest() {
		Uri uri = Uri.parse(mUniversity.getServiceUrl() + "/users/user").buildUpon()
				.appendQueryParameter("fields", "id|titles|first_name|last_name|student_number|student_status|student_programmes[id|programme|status|admission_date]")
				.build();

		return new OAuthRequest(Verb.GET, uri.toString());
	}
}
