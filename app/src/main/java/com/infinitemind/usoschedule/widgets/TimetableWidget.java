package com.infinitemind.usoschedule.widgets;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.widget.RemoteViews;

import com.infinitemind.usoschedule.R;
import com.infinitemind.usoschedule.activities.SplashActivity;
import com.infinitemind.usoschedule.utils.AppPreferences;
import com.infinitemind.usoschedule.views.WeekViewStatic;

public class TimetableWidget extends AppWidgetProvider {

	private static int WIDTH, HEIGHT;
	private static int dayOffset;

	static void updateAppWidget(Context context, AppWidgetManager appWidgetManager, int id) {
		RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.widget_timetable);

		getSize(appWidgetManager, id, context);

		views.setImageViewBitmap(R.id.timetable, constructTimetable(context));
		views.setOnClickPendingIntent(R.id.timetable, PendingIntent.getActivity(context, 0, new Intent(context, SplashActivity.class), PendingIntent.FLAG_UPDATE_CURRENT));
		views.setOnClickPendingIntent(R.id.arrowLeft, PendingIntent.getBroadcast(context, 1, new Intent(context, TimetableWidget.class).putExtra("dir", -1), PendingIntent.FLAG_UPDATE_CURRENT));
		views.setOnClickPendingIntent(R.id.arrowRight, PendingIntent.getBroadcast(context, 2, new Intent(context, TimetableWidget.class).putExtra("dir", 1), PendingIntent.FLAG_UPDATE_CURRENT));

		appWidgetManager.updateAppWidget(id, views);
	}

	private static void getSize(AppWidgetManager appWidgetManager, int id, Context context) {
		Bundle mAppWidgetOptions = appWidgetManager.getAppWidgetOptions(id);
		DisplayMetrics metrics = context.getResources().getDisplayMetrics();
		WIDTH = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, mAppWidgetOptions.getInt(AppWidgetManager.OPTION_APPWIDGET_MIN_WIDTH), metrics);
		HEIGHT = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, mAppWidgetOptions.getInt(AppWidgetManager.OPTION_APPWIDGET_MAX_HEIGHT), metrics);
	}

	private static Bitmap constructTimetable(Context context) {
		Bitmap output = Bitmap.createBitmap(WIDTH, HEIGHT, Bitmap.Config.ARGB_8888);
		Canvas canvas = new Canvas(output);
		AppPreferences prefs = new AppPreferences(context);
		boolean nM = prefs.isNightModeEnabled();

		WeekViewStatic.Data data = new WeekViewStatic.Data();
		data.WIDTH = WIDTH;
		data.HEIGHT = HEIGHT;
		data.dayOffset = dayOffset;
		data.firstDay = prefs.getWidgetFirstDay() + 1;
		data.lastDay = prefs.getWidgetLastDay() + 1;
		data.headerColor = data.futureBackgroundColor = context.getResources().getColor(nM ? R.color.colorDarkGrey : R.color.weekendBackgroundColor);
		data.weekendBackgroundColor = context.getResources().getColor(nM ? R.color.colorGrey : R.color.futureBackgroundColor);
		data.pastBackgroundColor = context.getResources().getColor(nM ? R.color.black : R.color.pastBackgroundColor);
		data.todayBackgroundColor = context.getResources().getColor(nM ? R.color.colorSemiPrimary : R.color.colorSemiPrimaryLight);
		data.eventTextColor = context.getResources().getColor(nM ? R.color.colorText1 : R.color.colorText2);
		data.headerTextColor = context.getResources().getColor(nM ? R.color.colorText2 : R.color.colorText1);
		WeekViewStatic.init(data, context);
		WeekViewStatic.draw(data, canvas);

		return output;
	}

	@Override public void onReceive(Context context, Intent intent) {
		super.onReceive(context, intent);

		int dir = intent.getIntExtra("dir", 0);
		if(dir == -1) dayOffset -= 7;
		else if(dir == 1) dayOffset += 7;

		int[] ids = AppWidgetManager.getInstance(context).getAppWidgetIds(new ComponentName(context, TimetableWidget.class));
		if(ids.length > 0) updateAppWidget(context, AppWidgetManager.getInstance(context), ids[0]);
	}

	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] ids) {
		for(int appWidgetId : ids) updateAppWidget(context, appWidgetManager, appWidgetId);
	}
}