package com.infinitemind.usoschedule.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.infinitemind.usoschedule.R;
import com.infinitemind.usoschedule.model.User;

import java.util.ArrayList;

public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.DataObjectHolder> {
	private static final int defaultMinimizedAmount = 3;
	private boolean minimizable, minimized;
	private final ArrayList<User> users;
	private MyClickListener myClickListener;
	private Context context;

	class DataObjectHolder extends RecyclerView.ViewHolder {
		final AppCompatTextView title;
		final AppCompatTextView subtitle;
		final AppCompatTextView footnote;
		final AppCompatTextView button;
		final ViewGroup background;
		final View divider;

		DataObjectHolder(View itemView) {
			super(itemView);
			title = itemView.findViewById(R.id.title);
			subtitle = itemView.findViewById(R.id.subtitle);
			footnote = itemView.findViewById(R.id.footnote);
			background = itemView.findViewById(R.id.background);
			divider = itemView.findViewById(R.id.divider);
			button = itemView.findViewById(R.id.button);

			if(background != null) {
				background.setOnClickListener(view -> myClickListener.onClick(getAdapterPosition()));
				background.setOnLongClickListener(view -> myClickListener.onLongClick(getAdapterPosition()));
			}
			if(button != null) button.setOnClickListener(view -> myClickListener.onShowMoreClick());
		}

	}

	public void setOnClickListener(MyClickListener myClickListener) {
		this.myClickListener = myClickListener;
	}

	private UserListAdapter(ArrayList<User> users) {
		this.users = users;
		setHasStableIds(true);
	}

	public UserListAdapter(ArrayList<User> users, boolean minimizable) {
		this(users);
		this.minimizable = this.minimized = minimizable;
	}

	public void setMinimized(boolean minimized) {
		this.minimized = minimized;
		notifyDataSetChanged();
	}

	public boolean isMinimized() {
		return minimized;
	}

	@Override
	public long getItemId(int position) {
		return getItemViewType(position) == 0 ? users.get(position).id.hashCode() : getItemViewType(position);
	}

	@Override
	public int getItemViewType(int position) {
		return minimizable && users.size() > defaultMinimizedAmount && position == getItemCount() - 1 ? 1 : 0;
	}

	@NonNull
	@Override
	public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		return new DataObjectHolder(LayoutInflater.from(context = parent.getContext()).inflate(viewType == 0 ? R.layout.item_extended_list : R.layout.item_more_list, parent, false));
	}

	@Override
	public void onBindViewHolder(@NonNull final DataObjectHolder holder, int position) {
		if(getItemViewType(position) == 0) {
			String output = users.get(position).matched;
			if(output != null) {
				User.Title titles = users.get(position).titles;
				if(titles != null) {
					if(titles.before != null && !titles.before.isEmpty())
						output = String.format("%s %s", titles.before, output);
					if(titles.after != null && !titles.after.isEmpty())
						output = String.format("%s %s", output, titles.after);
				}
				holder.title.setText(Html.fromHtml(output));
			} else holder.title.setText(users.get(position).getUserName());

			String email = users.get(position).email;
			if(email != null && !email.isEmpty()) {
				holder.subtitle.setVisibility(View.VISIBLE);
				holder.subtitle.setText(email);
			} else holder.subtitle.setVisibility(View.GONE);

			holder.footnote.setVisibility(View.GONE);

			holder.divider.setVisibility(position == users.size() - 1 && (users.size() <= defaultMinimizedAmount || !minimizable) ? View.GONE : View.VISIBLE);
		} else holder.button.setText(context.getResources().getString(isMinimized() ? R.string.show_more : R.string.show_less));
	}

	@Override
	public int getItemCount() {
		if(users.size() > defaultMinimizedAmount && minimizable) {
			if(minimized) return defaultMinimizedAmount + 1;
			else return users.size() + 1;
		} else return users.size();
	}

	public interface MyClickListener {
		void onClick(int position);
		default boolean onLongClick(int position) {return false;}
		default void onShowMoreClick() {}
	}
}