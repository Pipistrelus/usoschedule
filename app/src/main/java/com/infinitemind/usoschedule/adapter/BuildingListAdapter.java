package com.infinitemind.usoschedule.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.infinitemind.usoschedule.R;
import com.infinitemind.usoschedule.model.Building;

import java.util.ArrayList;

public class BuildingListAdapter extends RecyclerView.Adapter<BuildingListAdapter.DataObjectHolder> {
	private final ArrayList<Building> buildings;
	private MyClickListener myClickListener;

	class DataObjectHolder extends RecyclerView.ViewHolder {
		final AppCompatTextView title;
		final AppCompatTextView subtitle;
		final AppCompatTextView footnote;
		final View divider;
		final ViewGroup background;

		DataObjectHolder(View itemView) {
			super(itemView);
			background = itemView.findViewById(R.id.background);
			title = itemView.findViewById(R.id.title);
			subtitle = itemView.findViewById(R.id.subtitle);
			footnote = itemView.findViewById(R.id.footnote);

			divider = itemView.findViewById(R.id.divider);

			background.setOnClickListener(view -> myClickListener.onBuildingClick(getAdapterPosition()));
		}
	}

	public void setOnClickListener(MyClickListener myClickListener) {
		this.myClickListener = myClickListener;
	}

	public BuildingListAdapter(ArrayList<Building> buildings) {
		this.buildings = buildings;
		setHasStableIds(true);
	}

	@Override
	public long getItemId(int position) {
		return buildings.get(position).id.hashCode();
	}

	@NonNull
	@Override
	public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		return new DataObjectHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_extended_list, parent, false));
	}

	@Override
	public void onBindViewHolder(@NonNull final DataObjectHolder holder, int position) {
		String postal_address = buildings.get(position).postal_address;
		holder.title.setText(buildings.get(position).name.getName());
		holder.subtitle.setText(postal_address);
		holder.footnote.setText(buildings.get(position).id);

		holder.subtitle.setVisibility(postal_address == null || postal_address.isEmpty() ? View.GONE : View.VISIBLE);
		holder.divider.setVisibility(position == getItemCount() - 1 ? View.GONE : View.VISIBLE);

	}

	@Override
	public int getItemCount() {
		return buildings.size();
	}

	public interface MyClickListener {
		void onBuildingClick(int position);
	}
}