package com.infinitemind.usoschedule.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.infinitemind.usoschedule.R;
import com.infinitemind.usoschedule.model.University;

import java.util.ArrayList;

public class UniversityListAdapter extends RecyclerView.Adapter<UniversityListAdapter.DataObjectHolder> {
	private final ArrayList<University> universities;

	class DataObjectHolder extends RecyclerView.ViewHolder {
		final AppCompatTextView name;
		final AppCompatTextView location;

		DataObjectHolder(View itemView) {
			super(itemView);
			name = itemView.findViewById(R.id.name);
			location = itemView.findViewById(R.id.location);
		}
	}

	public UniversityListAdapter(ArrayList<University> universities) {
		this.universities = universities;
		setHasStableIds(true);
	}

	@Override
	public long getItemId(int position) {
		return universities.get(position).hashCode();
	}

	@NonNull
	@Override
	public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		return new DataObjectHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_university_list, parent, false));
	}

	@Override
	public void onBindViewHolder(@NonNull final DataObjectHolder holder, int position) {
		holder.name.setText(universities.get(position).name);
		holder.location.setText(universities.get(position).location);
	}

	@Override
	public int getItemCount() {
		return universities.size();
	}
}