package com.infinitemind.usoschedule.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.infinitemind.usoschedule.R;
import com.infinitemind.usoschedule.model.StudentProgramme;

import java.util.ArrayList;

public class ProgrammeListAdapter extends RecyclerView.Adapter<ProgrammeListAdapter.DataObjectHolder> {
	private final ArrayList<StudentProgramme> studentProgrammes;
	private Context context;

	class DataObjectHolder extends RecyclerView.ViewHolder {
		final AppCompatTextView title;
		final AppCompatTextView subtitle;
		final AppCompatTextView footnote;
		final View divider;

		DataObjectHolder(View itemView) {
			super(itemView);
			title = itemView.findViewById(R.id.title);
			subtitle = itemView.findViewById(R.id.subtitle);
			footnote = itemView.findViewById(R.id.footnote);
			divider = itemView.findViewById(R.id.divider);

			itemView.findViewById(R.id.background).setClickable(false);
		}
	}

	public ProgrammeListAdapter(ArrayList<StudentProgramme> studentProgrammes) {
		this.studentProgrammes = studentProgrammes;
		setHasStableIds(true);
	}

	@Override
	public long getItemId(int position) {
		return studentProgrammes.get(position).hashCode();
	}

	@NonNull
	@Override
	public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		return new DataObjectHolder(LayoutInflater.from(context = parent.getContext()).inflate(R.layout.item_extended_list, parent, false));
	}

	@Override
	public void onBindViewHolder(@NonNull final DataObjectHolder holder, int position) {
		holder.title.setText(studentProgrammes.get(position).programme.description.getName());
		try {
			holder.subtitle.setText(context.getResources().getString(context.getResources().getIdentifier(studentProgrammes.get(position).status, "string", context.getPackageName())));
		} catch(Exception ex) {
			holder.subtitle.setText(studentProgrammes.get(position).status);
		}
		holder.subtitle.setTextColor(context.getResources().getColor(studentProgrammes.get(position).status.equals("active") ? R.color.colorGreen : R.color.colorRed));
		holder.footnote.setText(studentProgrammes.get(position).admission_date);

		holder.divider.setVisibility(position == getItemCount() - 1 ? View.GONE : View.VISIBLE);
	}

	@Override
	public int getItemCount() {
		return studentProgrammes.size();
	}
}