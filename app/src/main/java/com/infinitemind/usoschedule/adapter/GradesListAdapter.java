package com.infinitemind.usoschedule.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.infinitemind.usoschedule.R;
import com.infinitemind.usoschedule.model.CourseGrade;

import java.util.HashMap;
import java.util.List;

public class GradesListAdapter extends RecyclerView.Adapter<GradesListAdapter.DataObjectHolder> {
	private final List<CourseGrade> grades;
	private HashMap<String, String> courseNames;
	private Context context;

	class DataObjectHolder extends RecyclerView.ViewHolder {
		final AppCompatTextView subtitle;
		final AppCompatTextView title;
		final AppCompatTextView footnote;
		final View divider;

		DataObjectHolder(View itemView) {
			super(itemView);
			subtitle = itemView.findViewById(R.id.subtitle);
			title = itemView.findViewById(R.id.title);
			footnote = itemView.findViewById(R.id.footnote);
			divider = itemView.findViewById(R.id.divider);

			itemView.findViewById(R.id.background).setClickable(false);
		}

	}

	public GradesListAdapter(List<CourseGrade> grades) {
		this.grades = grades;
		setHasStableIds(true);
	}

	public GradesListAdapter(List<CourseGrade> grades, HashMap<String, String> courseNames) {
		this.grades = grades;
		this.courseNames = courseNames;
		setHasStableIds(true);
	}

	@Override
	public long getItemId(int position) {
		return grades.get(position).exam_id;
	}

	@NonNull
	@Override
	public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		return new DataObjectHolder(LayoutInflater.from(context = parent.getContext()).inflate(R.layout.item_extended_list, parent, false));
	}

	@Override
	public void onBindViewHolder(@NonNull final DataObjectHolder holder, int position) {
		CourseGrade grade = grades.get(position);
		if(grade == null) return;

		if(grade.value_symbol.equals("-")) {
			holder.title.setText("-");
			holder.subtitle.setText("");
			holder.footnote.setText("");
		} else {
			holder.title.setText(grade.value_symbol);
			holder.title.setTextColor(context.getResources().getColor(grade.value_symbol.contains("2,0") || grade.value_symbol.contains("2,5") ? R.color.colorRed : R.color.colorGreen));

			if(courseNames == null) {
				holder.subtitle.setText(context.getResources().getString(R.string.sessionNumber, grade.exam_session_number));
				holder.footnote.setText(grade.date_modified.substring(0, grade.date_modified.indexOf(" ")));
			} else {
				holder.subtitle.setText(courseNames.get(grade.course_id));
				holder.footnote.setText(context.getResources().getString(R.string.sessionNumber, grade.exam_session_number));
			}
		}

		holder.divider.setVisibility(position == getItemCount() - 1 ? View.GONE : View.VISIBLE);
	}

	@Override
	public int getItemCount() {
		return grades.size();
	}
}