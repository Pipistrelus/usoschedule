package com.infinitemind.usoschedule.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.infinitemind.usoschedule.R;
import com.infinitemind.usoschedule.model.Filter;
import com.infinitemind.usoschedule.views.Chip;

import java.util.ArrayList;

public class FilterListAdapter extends RecyclerView.Adapter<FilterListAdapter.DataObjectHolder> {
	private static final int defaultMinimizedAmount = 3;
	private final boolean minimizable;
	private boolean minimized;
	private OnCheckListener onCheckListener;
	private final ArrayList<Filter> filters;
	private Context context;

	class DataObjectHolder extends RecyclerView.ViewHolder {
		final Chip chip;

		DataObjectHolder(View itemView) {
			super(itemView);
			chip = itemView.findViewById(R.id.chip);

			chip.setOnCheckedChangeListener(checked -> onCheckListener.onCheck(getAdapterPosition(), checked));
		}
	}

	public FilterListAdapter(ArrayList<Filter> filters, boolean minimized) {
		this.filters = filters;
		this.minimized = this.minimizable = minimized;
	}

	public FilterListAdapter(ArrayList<Filter> filters) {
		this.filters = filters;
		this.minimized = this.minimizable = false;
	}

	public void setOnCheckListener(OnCheckListener onCheckListener) {
		this.onCheckListener = onCheckListener;
	}

	public void setMinimized(boolean minimized) {
		this.minimized = minimized;
	}

	public boolean isMaximized() {
		return !minimized;
	}

	@Override
	public long getItemId(int position) {
		return getItemViewType(position) == 1 ? filters.get(position).getId().hashCode() : -1;
	}

	@Override
	public int getItemViewType(int position) {
		return minimizable && minimized && filters.size() > defaultMinimizedAmount && position == getItemCount() - 1 ? 1 : 0;
	}

	@NonNull
	@Override
	public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		return new DataObjectHolder(LayoutInflater.from(context = parent.getContext()).inflate(R.layout.item_filter_list, parent, false));
	}

	@Override
	public void onBindViewHolder(@NonNull final DataObjectHolder holder, int position) {
		if(getItemViewType(position) == 0) {
			holder.chip.setText(filters.get(position).getValue());
			holder.chip.setCheckable(filters.get(position).isCheckable());
			holder.chip.setChecked(filters.get(position).isChecked());
			holder.chip.setCheckedColor(filters.get(position).getColor());
		} else {
			holder.chip.setText(context.getResources().getString(R.string.show_more));
			holder.chip.setCheckable(false);
			holder.chip.setChecked(false);
			holder.chip.setCheckedColor(context.getResources().getColor(R.color.colorGrey));
		}
	}

	@Override
	public int getItemCount() {
		return minimized ? Math.min(defaultMinimizedAmount + 1, filters.size()) : filters.size();
	}

	public interface OnCheckListener {
		void onCheck(int position, boolean checked);
	}
}