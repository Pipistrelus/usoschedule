package com.infinitemind.usoschedule.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.infinitemind.usoschedule.R;
import com.infinitemind.usoschedule.model.Group;

import java.util.List;

public class GroupsListAdapter extends RecyclerView.Adapter<GroupsListAdapter.DataObjectHolder> {
	private final List<Group> groups;
	private MyClickListener myClickListener;
	private Context context;

	class DataObjectHolder extends RecyclerView.ViewHolder {
		final AppCompatTextView title;
		final AppCompatTextView subtitle;
		final AppCompatTextView footnote;
		final ViewGroup background;
		final View divider;

		DataObjectHolder(View itemView) {
			super(itemView);
			title = itemView.findViewById(R.id.title);
			subtitle = itemView.findViewById(R.id.subtitle);
			footnote = itemView.findViewById(R.id.footnote);
			background = itemView.findViewById(R.id.background);
			divider = itemView.findViewById(R.id.divider);

			background.setOnClickListener(view -> myClickListener.onClick(getAdapterPosition()));
		}

	}

	public void setOnClickListener(MyClickListener myClickListener) {
		this.myClickListener = myClickListener;
	}

	public GroupsListAdapter(List<Group> groups) {
		this.groups = groups;
		setHasStableIds(true);
	}

	@Override
	public long getItemId(int position) {
		return groups.get(position).course_id.hashCode();
	}

	@NonNull
	@Override
	public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		return new DataObjectHolder(LayoutInflater.from(context = parent.getContext()).inflate(R.layout.item_extended_list, parent, false));
	}

	@Override
	public void onBindViewHolder(@NonNull final DataObjectHolder holder, int position) {
		holder.title.setText(groups.get(position).course_name.getName());
		holder.subtitle.setText(context.getResources().getString(R.string.group_name, groups.get(position).group_number));
		holder.footnote.setText(groups.get(position).class_type.getName());

		holder.divider.setVisibility(position == getItemCount() - 1 ? View.GONE : View.VISIBLE);
	}

	@Override
	public int getItemCount() {
		return groups.size();
	}

	public interface MyClickListener {
		void onClick(int position);
	}
}