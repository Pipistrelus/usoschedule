package com.infinitemind.usoschedule.clickListeners;

import com.infinitemind.usoschedule.model.Group;

public interface OnGroupClickListener {
	void onGroupClick(Group group);
}
