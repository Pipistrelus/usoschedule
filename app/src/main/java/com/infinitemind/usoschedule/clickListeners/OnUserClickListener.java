package com.infinitemind.usoschedule.clickListeners;

import com.infinitemind.usoschedule.model.User;

public interface OnUserClickListener {
	void onUserClick(User user);
}
