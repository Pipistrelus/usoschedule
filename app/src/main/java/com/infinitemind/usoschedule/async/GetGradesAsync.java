package com.infinitemind.usoschedule.async;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.reflect.TypeToken;
import com.infinitemind.usoschedule.api.StudentApi;
import com.infinitemind.usoschedule.model.CourseGrade;
import com.infinitemind.usoschedule.runnable.DataCallback;

import org.apache.commons.httpclient.HttpStatus;
import org.json.JSONObject;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class GetGradesAsync extends AsyncTask<Context, Void, List<CourseGrade>> {

    private final String termId;
    private final List<String> courseIds;
    private final DataCallback<List<CourseGrade>> userListener;

    public GetGradesAsync(String termId, List<String> courseIds, DataCallback<List<CourseGrade>> listener) {
        this.termId = termId;
        this.courseIds = courseIds;
        this.userListener = listener;
    }

    @Override
    protected List<CourseGrade> doInBackground(Context... contexts) {
        try {
            StudentApi mStudentApi = new StudentApi(contexts[0]);
            OAuthRequest request;
	        List<CourseGrade> grades = new ArrayList<>();

	        for(String courseId : courseIds) {
		        request = mStudentApi.getGradesRequest(termId, courseId);
		        Response response = mStudentApi.execute(request);
		        if(response != null && response.getCode() == HttpStatus.SC_OK) {
		        	try {
				        JSONObject jsonObject = new JSONObject(response.getBody());
				        JSONObject course_grades = jsonObject.getJSONObject("course_grades");
				        Iterator<String> iterator = course_grades.keys();
				        while(iterator.hasNext()) {
					        String key = iterator.next();
					        CourseGrade grade = mStudentApi.getGson().fromJson(course_grades.getJSONObject(key).toString(), CourseGrade.class);
					        grade.counts_into_average = grade.counts_into_average || course_grades.getJSONObject(key).getString("counts_into_average").equalsIgnoreCase("T");
					        grades.add(grade);
					        grade.course_id = courseId;
				        }
			        } catch(Exception e) {
		        		Log.d("LOG!", e.getLocalizedMessage());
			        }
		        }
	        }
	        return grades;
        } catch(Exception e) {
        	Log.d("LOG!", e.getLocalizedMessage());
        }
        return null;
    }

    @Override
    protected void onPostExecute(List<CourseGrade> courseGrade) {
        super.onPostExecute(courseGrade);
        userListener.run(courseGrade);
    }
}
