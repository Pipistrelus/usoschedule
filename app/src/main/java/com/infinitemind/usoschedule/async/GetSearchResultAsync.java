package com.infinitemind.usoschedule.async;

import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.reflect.TypeToken;
import com.infinitemind.usoschedule.api.StudentApi;
import com.infinitemind.usoschedule.model.User;
import com.infinitemind.usoschedule.runnable.DataClassCallback;

import org.apache.commons.httpclient.HttpStatus;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class GetSearchResultAsync extends AsyncTask<Context, Void, List<User>> {

    private final DataClassCallback<List<User>> listener;
    private final String query;
	private final int start;

	public GetSearchResultAsync(String query, int start, DataClassCallback<List<User>> listener) {
        this.query = query;
		this.start = start;
		this.listener = listener;
    }

    @Override
    protected List<User> doInBackground(Context... contexts) {
        try {
            StudentApi mStudentApi = new StudentApi(contexts[0]);
            OAuthRequest request = mStudentApi.getFindingRequest(query, start);
            Response response = mStudentApi.execute(request);
            if(response != null && response.getCode() == HttpStatus.SC_OK) {
	            Temporary temporary = mStudentApi.getGson().fromJson(response.getBody(), Temporary.class);
	            ArrayList<User> users = new ArrayList<>();
	            for(Temporary.Item item : temporary.items) {
	            	item.user.matched = item.match;
		            users.add(item.user);
	            }
	            listener.setObject(temporary.next_page);
	            return users;
            }
        } catch(Exception ignored) {
        }
        return null;
    }

    private class Temporary {
		List<Item> items;
		boolean next_page;
    	class Item {
    		String match;
    		User user;
	    }
    }

    @Override
    protected void onPostExecute(List<User> users) {
        super.onPostExecute(users);
        if(users != null)
            listener.run(users);
    }
}
