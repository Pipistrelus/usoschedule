package com.infinitemind.usoschedule.async;

import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.reflect.TypeToken;
import com.infinitemind.usoschedule.api.StudentApi;
import com.infinitemind.usoschedule.model.User;
import com.infinitemind.usoschedule.runnable.DataCallback;

import org.apache.commons.httpclient.HttpStatus;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;

import java.lang.reflect.Type;

public class GetUserDetailAsync extends AsyncTask<Context, Void, User> {

	private final DataCallback<User> userListener;
	private String userId;

    public GetUserDetailAsync(String userId, DataCallback<User> listener) {
        this.userId = userId;
        this.userListener = listener;
    }

    public GetUserDetailAsync(DataCallback<User> listener) {
        this.userListener = listener;
    }

    @Override
    protected User doInBackground(Context... contexts) {
        try {
            StudentApi mStudentApi = new StudentApi(contexts[0]);
            OAuthRequest request;
	        if(userId != null) request = mStudentApi.getUserDetailRequest(userId);
	        else request = mStudentApi.getUserDetailRequest();
	        Response response = mStudentApi.execute(request);
            if(response != null && response.getCode() == HttpStatus.SC_OK)
                return mStudentApi.getGson().fromJson(response.getBody(), User.class);
        } catch(Exception ignored) {
        }
        return null;
    }

    @Override
    protected void onPostExecute(User user) {
        super.onPostExecute(user);
        userListener.run(user);
    }
}
