package com.infinitemind.usoschedule.async;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Pair;

import com.google.gson.reflect.TypeToken;
import com.infinitemind.usoschedule.api.StudentApi;
import com.infinitemind.usoschedule.model.Course;
import com.infinitemind.usoschedule.runnable.DataCallback;
import com.infinitemind.usoschedule.utils.AppPreferences;

import org.apache.commons.httpclient.HttpStatus;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

public class GetCourseScheduleAsync extends AsyncTask<Context, Void, List<Course>> {

	private final List<Pair<Calendar, Integer>> days;
	private final DataCallback<List<Course>> listener;
	private final String courseId;
	private final String termId;

	public GetCourseScheduleAsync(String termId, String courseId, List<Pair<Calendar, Integer>> days, DataCallback<List<Course>> listener) {
		this.termId = termId;
		this.courseId = courseId;
		this.days = days;
		this.listener = listener;
	}

	@Override
	protected List<Course> doInBackground(Context... contexts) {
		try {
			AppPreferences appPreferences = new AppPreferences(contexts[0]);
			StudentApi mStudentApi = new StudentApi(contexts[0]);
			OAuthRequest request;
			Response response;
			List<Course> courses = new ArrayList<>();

			for(Pair<Calendar, Integer> day : days) {
				request = mStudentApi.getTimetableForCourseRequest(termId, courseId, day.first, day.second);
				response = mStudentApi.execute(request);
				if(response != null && response.getCode() == HttpStatus.SC_OK)
					courses.addAll(setBreaks(appPreferences, mStudentApi.getGson().fromJson(response.getBody(), new TypeToken<List<Course>>() {}.getType())));
			}

			return courses;
		} catch(Exception ignored) { }
		return null;
	}

	private ArrayList<Course> setBreaks(AppPreferences appPreferences, List<Course> temp) {
		boolean breakNeeded = false;
		int breakLength = appPreferences.getBreakLength();

		ArrayList<Course> courses = new ArrayList<>(temp);
		Collections.sort(courses, (c1, c2) -> c1.start_time.compareTo(c2.start_time));
		for(int i = 0; i < courses.size() - 1; i++)
			if(courses.get(i).end_time.compareTo(courses.get(i + 1).start_time) == 0) {
				breakNeeded = true;
				break;
			}

		if(breakNeeded)
			for(Course c : courses) {
				Calendar d1 = Calendar.getInstance();
				d1.setTime(c.end_time);
				d1.add(Calendar.MINUTE, -breakLength);
				c.end_time.setTime(d1.getTimeInMillis());
			}
		return courses;
	}

	@Override
	protected void onPostExecute(List<Course> courses) {
		super.onPostExecute(courses);
		if(courses != null)
			listener.run(courses);
	}
}
