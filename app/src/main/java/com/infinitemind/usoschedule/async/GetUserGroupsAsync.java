package com.infinitemind.usoschedule.async;

import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.reflect.TypeToken;
import com.infinitemind.usoschedule.api.StudentApi;
import com.infinitemind.usoschedule.model.Group;
import com.infinitemind.usoschedule.model.Term;
import com.infinitemind.usoschedule.runnable.DataCallback;

import org.apache.commons.httpclient.HttpStatus;
import org.json.JSONObject;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;

import java.util.ArrayList;
import java.util.List;

public class GetUserGroupsAsync extends AsyncTask<Context, Void, List<List<Group>>> {

	private final DataCallback<List<List<Group>>> listListener;

	public GetUserGroupsAsync(DataCallback<List<List<Group>>> listListener) {
		this.listListener = listListener;
    }

    @Override
    protected List<List<Group>> doInBackground(Context... contexts) {
        try {
            StudentApi mStudentApi = new StudentApi(contexts[0]);
            OAuthRequest request = mStudentApi.getUserGroupsRequest();
            Response response = mStudentApi.execute(request);
            if(response != null && response.getCode() == HttpStatus.SC_OK) {
	            Temp json = mStudentApi.getGson().fromJson(response.getBody(), new TypeToken<Temp>() {}.getType());
	            JSONObject groups = new JSONObject(mStudentApi.getGson().toJson(json.groups));
	            if(!groups.keys().hasNext()) return null;
	            List<List<Group>> finalGroups = new ArrayList<>();
	            for(int i = 0; i < json.terms.length; i++) {
		            List<Group> temp = mStudentApi.getGson().fromJson(groups.getJSONArray(json.terms[i].id).toString(), new TypeToken<List<Group>>() {}.getType());
		            for(Group g : temp) g.term_ids = json.terms;
		            finalGroups.add(temp);
	            }
	            return finalGroups;
            }
        } catch(Exception ignored) { }
        return null;
    }

    class Temp {
		Object groups;
	    Term[] terms;
    }

    @Override
    protected void onPostExecute(List<List<Group>> groups) {
        super.onPostExecute(groups);
        if(groups != null) listListener.run(groups);
    }
}
