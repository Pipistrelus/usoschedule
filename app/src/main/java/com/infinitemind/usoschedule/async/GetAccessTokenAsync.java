package com.infinitemind.usoschedule.async;

import android.content.Context;
import android.os.AsyncTask;

import com.infinitemind.usoschedule.utils.AppPreferences;
import com.infinitemind.usoschedule.model.Universities;
import com.infinitemind.usoschedule.utils.OAuthHelper;

import org.scribe.model.Token;
import org.scribe.model.Verifier;

public class GetAccessTokenAsync extends AsyncTask<Void, Void, Token> {

    private final Token token;
    private final String verifier;
    private final Runnable callback;
    private final AppPreferences mAppPrefs;

    public GetAccessTokenAsync(Context context, Token token, String verifier, Runnable callback) {
        this.token = token;
        this.verifier = verifier;
        this.callback = callback;
        mAppPrefs = new AppPreferences(context);
    }

    @Override
    protected Token doInBackground(Void... params) {
        return OAuthHelper.getService(Universities.getUniversity(mAppPrefs.getUniversityId())).getAccessToken(token, new Verifier(this.verifier));
    }

    @Override
    protected void onPostExecute(Token token) {
        super.onPostExecute(token);
        mAppPrefs.edit().setAccessToken(token);
        callback.run();
    }
}
