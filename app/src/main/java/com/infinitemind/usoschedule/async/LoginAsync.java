package com.infinitemind.usoschedule.async;

import android.content.Context;
import android.os.AsyncTask;

import com.infinitemind.usoschedule.utils.AppPreferences;
import com.infinitemind.usoschedule.model.Universities;
import com.infinitemind.usoschedule.model.University;
import com.infinitemind.usoschedule.utils.OAuthHelper;
import com.infinitemind.usoschedule.runnable.DataCallback;

import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

public class LoginAsync extends AsyncTask<Void, Void, String> {

    private final University mUniversity;
    private final AppPreferences mAppPrefs;
    private final DataCallback<String> callback;

    public LoginAsync(Context context, DataCallback<String> callback) {
        this.callback = callback;
        mAppPrefs = new AppPreferences(context);
        mUniversity = Universities.getUniversity(mAppPrefs.getUniversityId());
    }

    @Override
    protected String doInBackground(Void... params) {
        try {
            OAuthService service = OAuthHelper.getService(mUniversity);
            Token requestToken = service.getRequestToken();
            mAppPrefs.edit().setTokenResponse(requestToken);
            return service.getAuthorizationUrl(requestToken);
        } catch (Exception ignore) {}
        return null;
    }

    @Override
    protected void onPostExecute(String authorizationUrl) {
        super.onPostExecute(authorizationUrl);
        callback.run(authorizationUrl);
    }
}
