package com.infinitemind.usoschedule.async;

import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.infinitemind.usoschedule.api.StudentApi;
import com.infinitemind.usoschedule.model.Building;
import com.infinitemind.usoschedule.model.LatLong;
import com.infinitemind.usoschedule.model.Name;
import com.infinitemind.usoschedule.runnable.DataCallback;

import org.apache.commons.httpclient.HttpStatus;
import org.json.JSONObject;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;

import java.util.ArrayList;
import java.util.List;

public class GetBuildingsAsync extends AsyncTask<Context, Void, List<Building>> {

	private final String id;
	private final DataCallback<List<Building>> listListener;

	public GetBuildingsAsync(DataCallback<List<Building>> listListener) {
		this.id = null;
		this.listListener = listListener;
    }

	public GetBuildingsAsync(String id, DataCallback<List<Building>> listListener) {
		this.id = id;
		this.listListener = listListener;
	}

    @Override
    protected List<Building> doInBackground(Context... contexts) {
        try {
            StudentApi mStudentApi = new StudentApi(contexts[0]);
            OAuthRequest request = id == null ? mStudentApi.getBuildingsRequest() : mStudentApi.getBuildingRequest(id);
            Response response = mStudentApi.execute(request);
            if(response != null && response.getCode() == HttpStatus.SC_OK) {
	            List<Object> temp;
	            if(id == null)
		           temp = mStudentApi.getGson().fromJson(response.getBody(), new TypeToken<List<Object>>() {}.getType());
	            else {
	            	temp = new ArrayList<>();
	            	temp.add(mStudentApi.getGson().fromJson(response.getBody(), Object.class));
	            }
	            List<Building> buildings = new ArrayList<>();
	            for(int i = 0; i < temp.size(); i++) {
		            JSONObject o = new JSONObject(new Gson().toJson(temp.get(i)));
	            	Building b = new Building();
	            	b.id = o.optString("id");
		            JSONObject name = o.optJSONObject("name");
		            if(name != null) b.name = mStudentApi.getGson().fromJson(name.toString(), Name.class);
		            JSONObject campus_name = o.optJSONObject("campus_name");
		            if(campus_name != null) b.campus_name = mStudentApi.getGson().fromJson(campus_name.toString(), Name.class);
	            	b.postal_address = o.optString("postal_address");
	            	b.location = new LatLong();
		            JSONObject location = o.optJSONObject("location");
		            if(location != null) {
			            b.location.lat = (float) location.getDouble("lat");
			            b.location.lng = (float) location.getDouble("long");
		            } else b.location = null;
		            buildings.add(b);
	            }
	            return buildings;
            }
        } catch(Exception ignored) { }
        return null;
    }

    @Override
    protected void onPostExecute(List<Building> buildings) {
        super.onPostExecute(buildings);
        listListener.run(buildings);
    }
}
